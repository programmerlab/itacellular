<?php

/*-----------------------------------------------------------------------------------*/
/* Start gomysites Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// Set path to gomyFramework and theme specific functions
$functions_path = get_template_directory() . '/functions/';
$includes_path = get_template_directory() . '/includes/';

// gomyFramework
require_once ($functions_path . 'admin-init.php' );			// Framework Init

// Theme specific functionality
require_once ($includes_path . 'theme-options.php' ); 		// Options panel settings and custom settings
require_once ($includes_path . 'theme-functions.php' ); 		// Custom theme functions
require_once ($includes_path . 'theme-plugins.php' );		// Theme specific plugins integrated in a theme
require_once ($includes_path . 'theme-actions.php' );		// Theme actions & user defined hooks
require_once ($includes_path . 'theme-comments.php' ); 		// Custom comments/pingback loop
require_once ($includes_path . 'theme-js.php' );				// Load javascript in wp_head
require_once ($includes_path . 'sidebar-init.php' );			// Initialize widgetized areas
require_once ($includes_path . 'theme-widgets.php' );		// Theme widgets

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/

// Force add main VC CSS
add_action('wp_enqueue_scripts', 'force_js_composer_front_load');
function force_js_composer_front_load() {
    wp_enqueue_style('js_composer_front');
}


add_action('wp_head', 'gomysites_add_css');
function gomysites_add_css() {
    if ( ! is_singular() ) {
        $id = get_the_ID(); // here you should change id to ID, of your post/page in loop
        if ( $id ) {
            $shortcodes_custom_css = get_post_meta( $id, '_wpb_shortcodes_custom_css', true );
            if ( ! empty( $shortcodes_custom_css ) ) {
                echo '<style type="text/css" data-type="vc_shortcodes-custom-css-'.$id.'">';
                echo $shortcodes_custom_css;
                echo '</style>';
            }
        }
    }
}

/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/
?>