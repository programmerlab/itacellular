<?php get_header(); ?>
<?php global $gomy_options; ?>
					
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h1 class="title"><?php the_title(); ?></h1>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div><!-- end title section -->
	
	
</div><!-- end top section -->

	
	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
			
			<div id="post-entries">
	            <div class="nav-prev fl"><?php previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title' ) ?></div>
	            <div class="nav-next fr"><?php next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' ) ?></div>
	            <div class="fix"></div>
	        </div><!-- #post-entries -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?>
	
	
<div id="main-content"><!-- start main content area -->

    <div class="col-full"><!--start content-->
	    
		<div id="main" class="col-full"><!--start main-->
			
			<?php $outofstock = get_post_meta($post->ID, 'outofstock', true); ?>
			<?php $specialoffer = get_post_meta($post->ID, 'specialoffer', true); ?>
			
			<?php if ( $outofstock) { ?>
				<div class="relative">
					<?php if ( $outofstock ) { ?><?php echo $outofstock; ?><?php } // End IF Statement ?>
				</div>
			<?php } ?><!-- End If out of stock -->
			
			
			<?php if ( $specialoffer) { ?>
				<div class="relative">
					<?php if ( $specialoffer ) { ?><?php echo $specialoffer; ?><?php } // End IF Statement ?>
				</div>
			<?php } ?><!-- End If special offer -->
			
	    
	        <?php if (have_posts()) : $count = 0; ?>
	        <?php while (have_posts()) : the_post(); $count++; ?>
	        
				<div <?php post_class(); ?>>

               <?php $portfolio_gallery = $gomy_options[ 'gomy_portfolio_gallery' ] == 'true'; ?>
               
               <?php echo gomy_embed( 'width=250' ); ?>
               
                <?php if ( $portfolio_gallery && !gomy_embed('')) { ?>
                	<div id="gallery">
                	
					<?php
						$gallery = do_shortcode('[gallery size="thumbnail" columns="4"]');
						
						if ( $gallery ) {
						
							// include('includes/gallery.php'); // Photo gallery
							
							$tpl_gallery = '';
							$tpl_gallery = locate_template( array( 'includes/gallery.php' ) );
							
							if ( $tpl_gallery ) {
							
								include( $tpl_gallery );
							
							} // End IF Statement
							
						} else {
							gomy_image('key=portfolio-image&width=250&class=portfolio-img');  
						}
					?>
					</div>
					
					<?php } elseif ( !gomy_embed('')) { ?><!-- End If portfolio_gallery -->
					
					<div id="gallery">
					<div class="entry">
                    	<?php gomy_image('key=portfolio-image&width=250&class=portfolio-img'); ?>
					</div>
					</div>
					<?php } ?>
					
					<?php $testimonial = get_post_meta($post->ID, 'testimonial', true); ?>
               		<?php $testimonial_author = get_post_meta($post->ID, 'testimonial_author', true); ?>
               		<?php $url = get_post_meta($post->ID, 'url', true); ?>

					<div id="portfolio-content"><!-- start phone content -->
						<h2><?php the_title(); ?></h2>
						
						<?php /* the_tags( '<p class="tags">'.__( '', 'gomysites' ), ' ', '</p>' ); */ ?>
						
                    	<div class="entry">	
	                		<?php the_content(); ?>
	                		<?php if ( $url ) { ?><a class="button" href="<?php echo $url; ?>"><?php _e( 'Visit Website', 'gomysites' ); ?></a><?php } ?>
	               		</div><!-- /.entry -->
	               	
	               	
		               	<?php if ( $testimonial) { ?>               
		               	<div id="testimonial">
		               	<h3><?php _e( 'Client Testimonial', 'gomysites' ); ?></h3>
		               	<div class="feedback">
		               		<div class="quotes">
		               			<div class="quote">
	                        	<?php if ( $testimonial ) { ?><blockquote><p><?php echo $testimonial; ?></p></blockquote><?php } // End IF Statement ?>
	                        	<?php if ( $testimonial_author ) { ?><cite><?php echo $testimonial_author; ?></cite><?php } // End IF Statement ?>
	                        	<?php if ( $url ) { ?><a href="<?php echo $url; ?>"><?php echo $url; ?></a> &rarr;<?php } // End IF Statement ?>
	                        	</div><!-- end quote -->
	                        </div><!-- end quotes -->
	                    </div><!-- end feedback -->
	                    <div class="feedback-bottom"></div>
		               	</div>
		               	<?php } ?><!-- End If testimonial -->
		               	
		               	
		               	<div id="sidebar-details"><!--start sidebar -->
							<?php gomy_sidebar('sidebar-details'); ?>		           
						</div><!--end sidebar -->
	               	
	               	</div><!-- end phone content -->
	               	
					<div class="fix"></div>
					<?php edit_post_link( __('{ Edit }', 'gomysites'), '<span class="small">', '</span>' ); ?>
                </div><!-- end post -->
                                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'gomysites') ?></p>
                </div><!-- end post -->
            <?php endif; ?>  
	        
			</div><!--end main-->
				
    </div><!--end content-->
    
</div><!-- end main content area -->
		
<?php get_footer(); ?>