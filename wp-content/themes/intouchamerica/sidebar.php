<?php 
	// Don't display sidebar if full width
	global $gomy_options;
	if ( $gomy_options[ 'gomy_layout' ] != "layout-full" ) :
?>	
<div id="sidebar" class="col-right">

	<?php if ( gomy_active_sidebar( 'primary' ) ) : ?>
    <div class="primary">
		<?php gomy_sidebar( 'primary' ); ?>		           
	</div>        
	<?php endif; ?>    
	
</div><!-- /#sidebar -->

<?php endif; ?>