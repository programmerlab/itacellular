<?php
/*
Template Name: Portfolio
*/

	global $gomy_options;
	get_header();
?>
       
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h1 class="title">Cell Phones, Smartphones &amp; Accessories</h1>
		<?php if ( $description ) { ?><span class="blog-title-sep">&bull;</span><span class="description"><?php echo $description; ?></span><?php } ?>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div><!-- end title section -->
	 
	 
</div><!-- end top section -->


	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?>
	
	
<div id="main-content"><!-- start main content area -->	
	
    <div class="col-full"><!--start content-->

		<div id="main" class="fullwidth"><!--start main-->
		
		 			
		<?php
			if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }
			$args = array(
							'post_type' => 'portfolio', 
							'paged' => $paged, 
							'meta_key' => 'portfolio-image', 
							'posts_per_page' => -1
						);
			query_posts( $args );
			 
			rewind_posts();
		?>
				           
		    <div id="portfolio" class="col-full">
		    	
		    	<!-- Tags -->
				<?php
					$terms = $gomy_options['gomy_portfolio_tags'];
					if ( $terms ) {
						$terms_string = '';
						
						foreach ( explode( ', ', $terms ) as $t ) {
							$tag = trim( $t );
							$tag = str_replace (" ", "-", $tag);	
							$tag = str_replace ("/", "-", $tag);
							$tag = strtolower ( $tag );
							$terms_string .= ' <a rel="' . $tag . '" href="#' . $tag . '" title="' . ucwords( $t ) . '">' . ucwords( $t ) . '</a>';
						} // End FOREACH Loop
				?>
		    	<div id="port-tags">
		            <div class="fl">
		                <span class="port-cat"><?php _e('Select a category:', 'gomysites'); ?> <a href="#" rel="all" class="current"><?php _e( 'All','gomysites' ); ?></a><?php echo $terms_string; ?></span>
		            </div>
			      	<div class="fix"></div>
			    </div>
			      
				<?php } ?>
				<!-- /Tags -->
				<div class="portfolio">
		        <?php if ( have_posts() ) : while (have_posts()) : the_post(); $count++; ?>
		        <?php 
					// Portfolio tags class
					$porttag = ""; 
					$posttags = get_the_tags(); 
					if ($posttags) { 
						foreach($posttags as $tag) { 
							$tag = $tag->name;
							$tag = str_replace (" ", "-", $tag);	
							$tag = str_replace ("/", "-", $tag);
							$tag = strtolower ( $tag );
							$porttag .= $tag . ' '; 
						} 
					}
					
					$excerpt = gomy_text_trim( get_the_excerpt(), '12');
										
				?>
				
				
				<?php $outofstock = get_post_meta($post->ID, 'outofstock', true); ?>
				<?php $seniorfriendly = get_post_meta($post->ID, 'seniorfriendly', true); ?>
				<?php $specialoffer = get_post_meta($post->ID, 'specialoffer', true); ?>
				
					<div class="group post portfolio-img <?php echo trim( $porttag ); ?>">
	                    <?php if ( $outofstock) { ?>
							<?php if ( $outofstock ) { ?><?php echo $outofstock; ?><?php } // End IF Statement ?>
						<?php } ?><!-- End If out of stock -->
						
						<?php if ( $seniorfriendly) { ?>
							<?php if ( $seniorfriendly ) { ?><?php echo $seniorfriendly; ?><?php } // End IF Statement ?>
						<?php } ?><!-- End If senior friendly -->
						
						<?php if ( $specialoffer) { ?>
							<?php if ( $specialoffer ) { ?><?php echo $specialoffer; ?><?php } // End IF Statement ?>
						<?php } ?><!-- End If special offer -->
						
	                    <a title="<?php echo $caption; ?>" href="<?php the_permalink(); ?>" class="thumb">
							<?php gomy_image('key=portfolio-image&width=214&height=300&link=img&alt=' . esc_attr( get_the_title() ) . ''); ?>
	                    </a>
						<h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						
						<p><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">Click for more details &raquo;</a></p>
						
						<?php /* if ( $excerpt ) { ?><div class="excerpt"><?php echo $excerpt; ?></div><?php } */ ?>
					</div>
		        
		        <?php endwhile; else : ?>
		        
				<div class="post">
				     <p class="note">You need to setup the <strong>Portfolio</strong> options and select a category for your portfolio posts.</p>
                </div><!-- /.post -->
		        
		        <?php endif; ?>  
				</div>	        		        
			
		    </div><!-- /#portfolio -->
                                                            
            <?php gomy_pagenav(); ?>
		</div><!-- /#main -->

    </div><!-- /#content -->
 
</div><!-- /#content -->
		
<?php get_footer(); ?>