<?php
/*
Template Name: Deals
*/
?>

<?php get_header(); ?>
       
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h1 class="title"><?php the_title(); ?></h1>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div><!-- end title section -->


</div><!-- end top section -->


	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?>


<div id="main-content"><!-- start main content area -->
    <div class="col-full"><!--start content-->

		<div id="main" class="fullwidth"><!-- start #main -->

            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div <?php post_class(); ?>>
                    
                    <div class="entry">
	                	<?php the_content(); ?>
	               	</div><!-- /.entry -->

					<?php edit_post_link( __( '{ Edit }', 'gomysites' ), '<span class="small">', '</span>' ); ?>

                </div><!-- /.post -->
                                                    
			<?php endwhile; else: ?>
				<div <?php post_class(); ?>>
                	<p><?php _e( 'Sorry, no posts matched your criteria.', 'gomysites' ) ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- end #main -->
			
	</div><!-- end .col-full -->
</div><!-- end #main-content -->
		
<?php get_footer(); ?>