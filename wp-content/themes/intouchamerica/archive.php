<?php get_header(); ?>

	<?php if (have_posts()) : $count = 0; ?>
		
	<div id="title-container" class="col-full">
            <?php if (is_category()) { ?>
        	<span class="archive_header">
        		<span class="fl cat"><?php _e( 'Archive', 'gomysites' ); ?> | <?php echo single_cat_title(); ?></span>
        	</span>        
        
            <?php } elseif (is_day()) { ?>
            <span class="archive_header"><?php _e( 'Archive', 'gomysites' ); ?> | <?php the_time( get_option( 'date_format' ) ); ?></span>

            <?php } elseif (is_month()) { ?>
            <span class="archive_header"><?php _e( 'Archive', 'gomysites' ); ?> | <?php the_time( 'F, Y' ); ?></span>

            <?php } elseif (is_year()) { ?>
            <span class="archive_header"><?php _e( 'Archive', 'gomysites' ); ?> | <?php the_time( 'Y' ); ?></span>

            <?php } elseif (is_author()) { ?>
            <span class="archive_header"><?php _e( 'Archive by Author', 'gomysites' ); ?></span>

            <?php } elseif (is_tag()) { ?>
            <span class="archive_header"><?php _e( 'Tag Archives:', 'gomysites' ); ?> <?php echo single_tag_title( '', true); ?></span>
            
            <?php } ?>
            
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div>

</div><!-- end top section -->
	
	
	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?> 
	
            
    <div id="main-content"> 	

	    <div id="inner" class="col-full">
	            
			<div id="main" class="col-left">
			
	        <?php while (have_posts()) : the_post(); $count++; 
	                                                                    
	            			$ico_cal = $gomy_options[ 'gomy_post_calendar' ] == "true";
							$full_content = $gomy_options[ 'gomy_post_content' ] != "content";
						?>
						<!-- Post Starts -->
						    <div <?php post_class(); ?>>
						    
						    <?php if ( $full_content ) { if ( $ico_cal ) { ?>
						    <div class="ico-cal alignleft"><div class="ico-day"><?php the_time('d'); ?></div><div class="ico-month"><?php the_time('M'); ?></div></div>
						    <?php } else { gomy_image( 'width='.$gomy_options[ 'gomy_thumb_w' ].'&height='.$gomy_options[ 'gomy_thumb_h' ].'&class=thumbnail '.$gomy_options[ 'gomy_thumb_align' ]); }} ?>
						    	
						    	<div class="details" <?php if ( $ico_cal && $full_content ) { echo 'style="margin-left:52px;"'; } ?>>
						    	
					        	<h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
					        	
					            <?php gomy_post_meta(); ?>
					            
					            <div class="entry">
	                    <?php if ( $gomy_options[ 'gomy_post_content' ] == "content" ) the_content(__( 'Read More...', 'gomysites' )); else the_excerpt(); ?>
	                </div><!-- /.entry -->
	
	                <div class="post-more">      
	                	<?php if ( $gomy_options[ 'gomy_post_content' ] == "excerpt" ) { ?>
	                    <span class="read-more"><a href="<?php the_permalink() ?>" title="<?php esc_attr_e( 'Continue Reading &rarr;', 'gomysites' ); ?>"><?php _e( 'Continue Reading &rarr;', 'gomysites' ); ?></a></span>
	                    <?php } ?>
	                </div>
					           </div><!-- /.details -->
					        </div><!-- /.post -->
	            
	        <?php endwhile; else: ?>
	        
	            <div <?php post_class(); ?>>
	                <p><?php _e( 'Sorry, no posts matched your criteria.', 'gomysites' ) ?></p>
	            </div><!-- /.post -->
	        
	        <?php endif; ?>  
	    
				<?php gomy_pagenav(); ?>
	                
			</div><!-- /#main -->
	
	        <?php get_sidebar(); ?>
	
		</div><!-- /#inner -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>