<?php
/*
Template Name: Blog
*/
get_header();
global $gomy_options;
?>
    <!-- #content Starts -->
	<?php gomy_content_before(); ?>
    <?php 
    $blog_title = __( 'The Blog', 'gomysites' );
    $blog_description = '';
    
    if ( isset( $gomy_options['gomy_blog_title'] ) && ( $gomy_options['gomy_blog_title'] != '' ) ) {
		$blog_title = $gomy_options['gomy_blog_title'];
	}
 	if ( isset( $gomy_options['gomy_blog_description'] ) && ( $gomy_options['gomy_blog_description'] != '' ) ) { 
		$blog_description = $gomy_options['gomy_blog_description'];
	 }
	?>   
					
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h2 class="title"><?php echo stripslashes( $blog_title ); ?></h2>
		<?php if ( $blog_description != '' )  { ?>
		<span class="blog-title-sep">&bull;</span><span class="description"><?php echo stripslashes( $blog_description ); ?></span>
		<?php } ?>
		<?php get_template_part( 'search', 'form' ); ?>
	</div><!-- end title section -->
	
	
</div><!-- end top section -->


	<?php if ( isset( $gomy_options['gomy_breadcrumbs_show'] ) && $gomy_options['gomy_breadcrumbs_show'] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?> 
	
	

<div id="main-content"><!-- start main content area --> 	

    <div class="col-full"><!--start content-->
		           		
		<div id="main" class="col-left"><!--start main-->

        <?php
        	if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); } elseif ( get_query_var( 'page') ) { $paged = get_query_var( 'page' ); } else { $paged = 1; }
        	
        	query_posts( array( 'post_type' => 'post', 'paged' => $paged ) );
        	
        	if ( have_posts() ) { $count = 0; while ( have_posts() ) { the_post(); $count++; 
                                                                    
            			$ico_cal = $gomy_options['gomy_post_calendar'] == 'true';
						$full_content = $gomy_options['gomy_post_content'] != 'content';
					?>
						<!-- Post Starts -->
					    <div <?php post_class(); ?>>
					    
					    <?php if ( $full_content ) { if ( $ico_cal ) { ?>
					    <div class="ico-cal alignleft"><div class="ico-day"><?php the_time( 'd' ); ?></div><div class="ico-month"><?php the_time( 'M' ); ?></div></div>
					    <?php } else { gomy_image( 'width=' . $gomy_options['gomy_thumb_w'] . '&height=' . $gomy_options['gomy_thumb_h'] . '&class=thumbnail ' . $gomy_options['gomy_thumb_align'] ); } } ?>
					    	
					    	<div class="details" <?php if ( $ico_cal && $full_content ) { echo 'style="margin-left:52px;"'; } ?>>
					    	
				        	<h2 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				        	
				            <?php gomy_post_meta(); ?>
				            
				            <?php if ( $gomy_options[ 'gomy_post_content' ] != "content" ) gomy_image('width='.$gomy_options[ 'gomy_single_w' ].'&height='.$gomy_options[ 'gomy_single_h' ].'&class=thumbnail '.$gomy_options[ 'gomy_thumb_align' ]); ?>
				            
				            <div class="entry">
				            	<?php global $more; $more = 0; ?>
                    			<?php if ( isset( $gomy_options['gomy_post_content'] ) && $gomy_options['gomy_post_content'] == 'content' ) { the_content( __( 'Read More...', 'gomysites' ) ); } else { the_excerpt(); } ?>
                			</div><!-- /.entry -->

                <div class="post-more">      
                	<?php if ( isset( $gomy_options['gomy_post_content'] ) && $gomy_options['gomy_post_content'] == 'excerpt' ) { ?>
                    <span class="read-more"><a href="<?php the_permalink(); ?>" title="<?php esc_attr_e( 'Continue Reading &rarr;', 'gomysites' ); ?>"><?php _e( 'Continue Reading &rarr;', 'gomysites' ); ?></a></span>
                    <?php } ?>
                </div>
				           </div><!-- /.details -->
				        </div><!-- /.post -->
                                                
        <?php
        		}
        	} else {
        ?>
            <div <?php post_class(); ?>>
                <p><?php _e( 'Sorry, no posts matched your criteria.', 'gomysites' ); ?></p>
            </div><!-- /.post -->
        <?php } ?>
        <?php gomy_pagenav(); ?>
		<?php wp_reset_query(); ?>                

        </div><!--end main-->
            
		<?php get_sidebar(); ?>

	</div><!--end content-->
		
</div><!-- end main content area -->    
		
<?php get_footer(); ?>