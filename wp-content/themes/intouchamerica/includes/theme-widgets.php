<?php

/*---------------------------------------------------------------------------------*/
/* Loads all the .php files found in /includes/widgets/ directory */
/*---------------------------------------------------------------------------------*/

include( get_template_directory() . '/includes/widgets/widget-gomy-tabs.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-adspace.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-blogauthor.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-embed.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-flickr.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-search.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-twitter.php' );
//include( get_template_directory() . '/includes/widgets/widget-gomy-subscribe.php' );
include( get_template_directory() . '/includes/widgets/widget-gomy-feedback.php' );

/*---------------------------------------------------------------------------------*/
/* Deregister Default Widgets */
/*---------------------------------------------------------------------------------*/
if (!function_exists( 'gomy_deregister_widgets')) {
	function gomy_deregister_widgets(){
	    unregister_widget( 'WP_Widget_Search' );         
	}
}
add_action( 'widgets_init', 'gomy_deregister_widgets' );  


?>