<?php

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Styled Text Shortcode for primary use in Slides
- Modify gomy_breadcrumbs() Arguments Specific to this Theme
- Register WP Menus
- Page navigation
- Post Meta
- CPT Slides
- CPT Mini-Features
- CPT Portfolio - Phones
- CPT Feedback
- Slider Button Shortcode
- Subscribe & Connect
- Exclude Pages
- Get Post image attachments 

-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Styled Text Shortcode for primary use in Slides */
/*-----------------------------------------------------------------------------------*/

if ( ! function_exists( 'gomy_shortcode_styledtext' ) ) {
	function gomy_shortcode_styledtext ( $atts, $content = null ) {
		
		$defaults = array(
						'css' => ''
				 	);

		$atts = shortcode_atts( $defaults, $atts );
		
		extract( $atts );

		if ( $css != '' ) { $css = ' ' . $css; }
		
		$output = '<span class="styledtext' . $css . '">' . $content . '</span>' . "\n";
		
		return apply_filters( 'gomy_shortcode_styledtext', $output, $atts );
		
	} // End gomy_shortcode_styledtext()
}

add_shortcode( 'styled_text', 'gomy_shortcode_styledtext' );

/*-----------------------------------------------------------------------------------*/
/* Modify gomy_breadcrumbs() Arguments Specific to this Theme */
/*-----------------------------------------------------------------------------------*/

add_filter( 'gomy_breadcrumbs_args', 'gomy_filter_breadcrumbs_args', 10 );

if ( ! function_exists( 'gomy_filter_breadcrumbs_args' ) ) {
	function gomy_filter_breadcrumbs_args( $args ) {
	
		$args['separator'] = '&rarr;';
	
		return $args;
	
	} // End gomy_filter_breadcrumbs_args()
}

/*-----------------------------------------------------------------------------------*/
/* Register WP Menus */
/*-----------------------------------------------------------------------------------*/
if ( function_exists( 'wp_nav_menu') ) {
	add_theme_support( 'nav-menus' );
	register_nav_menus( array( 'primary-menu' => __( 'Primary Menu', 'gomysites' ) ) );
	register_nav_menus( array( 'top-menu' => __( 'Top Menu', 'gomysites' ) ) );
}


/*-----------------------------------------------------------------------------------*/
/* Page navigation */
/*-----------------------------------------------------------------------------------*/
if (!function_exists( 'gomy_pagenav')) {
	function gomy_pagenav() {

		global $gomy_options;

		// If the user has set the option to use simple paging links, display those. By default, display the pagination.
		if ( array_key_exists( 'gomy_pagination_type', $gomy_options ) && $gomy_options[ 'gomy_pagination_type' ] == 'simple' ) {
			if ( get_next_posts_link() || get_previous_posts_link() ) {
		?>

            <div class="nav-entries">
                <?php next_posts_link( '<span class="nav-prev fl">'. __( '<span class="meta-nav">&larr;</span> Older posts', 'gomysites' ) . '</span>' ); ?>
                <?php previous_posts_link( '<span class="nav-next fr">'. __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'gomysites' ) . '</span>' ); ?>
                <div class="fix"></div>
            </div>

		<?php
			} // ENDIF
			
		} else {
				
			gomy_pagination();
		
		} // ENDIF		 
	
	} 
}


/*-----------------------------------------------------------------------------------*/
/* Post Meta */
/*-----------------------------------------------------------------------------------*/

if (!function_exists('gomy_post_meta')) {
	function gomy_post_meta( ) {
?>
<p class="post-meta">
	<span class="post-author"><span class="small"><?php _e('By', 'gomysites') ?></span> <?php the_author_posts_link(); ?></span>
	<span class="post-meta-sep">&bull;</span>
    <span class="post-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
    <span class="post-meta-sep">&bull;</span>
    <span class="post-category"><?php the_category(', ') ?></span>
    <span class="post-meta-sep">&bull;</span>
    <span class="comments"><?php comments_popup_link(__( 'Leave a comment', 'gomysites' ), __( '1 Comment', 'gomysites' ), __( '% Comments', 'gomysites' )); ?></span>
    <?php edit_post_link( __('{ Edit }', 'gomysites'), '<span class="small">', '</span>' ); ?>
</p>
<?php
	}
}

/*-----------------------------------------------------------------------------------*/
/* Custom Post Type - Slides */
/*-----------------------------------------------------------------------------------*/

add_action('init', 'gomy_add_slides');
if ( !function_exists('gomy_add_infoboxes') ) {
	function gomy_add_slides() 
	{
	  $labels = array(
	    'name' => _x('Slides', 'post type general name', 'gomysites', 'gomysites'),
	    'singular_name' => _x('Slide', 'post type singular name', 'gomysites'),
	    'add_new' => _x('Add New', 'slide', 'gomysites'),
	    'add_new_item' => __('Add New Slide', 'gomysites'),
	    'edit_item' => __('Edit Slide', 'gomysites'),
	    'new_item' => __('New Slide', 'gomysites'),
	    'view_item' => __('View Slide', 'gomysites'),
	    'search_items' => __('Search Slides', 'gomysites'),
	    'not_found' =>  __('No slides found', 'gomysites'),
	    'not_found_in_trash' => __('No slides found in Trash', 'gomysites'), 
	    'parent_item_colon' => ''
	  );
	  $args = array(
	    'labels' => $labels,
	    'public' => false,
	    'publicly_queryable' => false,
	    'show_ui' => true, 
	    'query_var' => true,
	    'rewrite' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'menu_icon' => get_template_directory_uri() .'/includes/images/slides.png',
	    'menu_position' => null,
	    'supports' => array('title','editor','thumbnail'/*'author','thumbnail','excerpt','comments'*/)
	  ); 
	  register_post_type('slide',$args);
	}
}

/*-----------------------------------------------------------------------------------*/
/* Custom Post Type - Mini Features */
/*-----------------------------------------------------------------------------------*/

add_action('init', 'gomy_add_infoboxes');
if ( !function_exists('gomy_add_infoboxes') ) {
	function gomy_add_infoboxes() 
	{
	  $labels = array(
	    'name' => _x('Mini-Features', 'post type general name', 'gomysites'),
	    'singular_name' => _x('Mini-Feature', 'post type singular name', 'gomysites'),
	    'add_new' => _x('Add New', 'infobox', 'gomysites'),
	    'add_new_item' => __('Add New Mini-Feature', 'gomysites'),
	    'edit_item' => __('Edit Mini-Feature', 'gomysites'),
	    'new_item' => __('New Mini-Feature', 'gomysites'),
	    'view_item' => __('View Mini-Feature', 'gomysites'),
	    'search_items' => __('Search Mini-Features', 'gomysites'),
	    'not_found' =>  __('No Mini-Features found', 'gomysites'),
	    'not_found_in_trash' => __('No Mini-Features found in Trash', 'gomysites'), 
	    'parent_item_colon' => ''
	  );
	  
	  $infobox_rewrite = get_option('gomy_infobox_rewrite');
	  if(empty($infobox_rewrite)) $infobox_rewrite = 'infobox';
	  
	  $args = array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'query_var' => true,
	    'rewrite' => array('slug'=> $infobox_rewrite),
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'menu_icon' => get_template_directory_uri() .'/includes/images/box.png',
	    'menu_position' => null,
	    'supports' => array('title','editor',/*'author','thumbnail','excerpt','comments'*/)
	  ); 
	  register_post_type('infobox',$args);
	}
}

/*-----------------------------------------------------------------------------------*/
/* Custom Post Type - Portfolio - Phones */
/*-----------------------------------------------------------------------------------*/

add_action('init', 'gomy_add_portfolio');
if ( !function_exists('gomy_add_portfolio') ) {
	function gomy_add_portfolio() 
	{
	  $labels = array(
	    'name' => _x('Phones', 'post type general name', 'gomysites'),
	    'singular_name' => _x('Phone', 'post type singular name', 'gomysites'),
	    'add_new' => _x('Add New', 'slide', 'gomysites'),
	    'add_new_item' => __('Add New Phone', 'gomysites'),
	    'edit_item' => __('Edit Phone', 'gomysites'),
	    'new_item' => __('New Phone', 'gomysites'),
	    'view_item' => __('View Phones', 'gomysites'),
	    'search_items' => __('Search Phones', 'gomysites'),
	    'not_found' =>  __('No Phones found', 'gomysites'),
	    'not_found_in_trash' => __('No Phones found in Trash', 'gomysites'), 
	    'parent_item_colon' => ''
	  );
	  
	  $portfolioitems_rewrite = get_option('gomy_portfolioitems_rewrite');
	  if(empty($portfolioitems_rewrite)) $portfolioitems_rewrite = 'portfolio-items';
	  
	  $args = array(
	    'labels' => $labels,
	    'public' => false,
	    'publicly_queryable' => true, 
	    'show_in_nav_menus' => true,
	    'exclude_from_search' => false,    
		'_builtin' => false,
	    'show_ui' => true, 
	    'query_var' => true,
	    'rewrite' => array('slug'=> $portfolioitems_rewrite),
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'menu_icon' => get_template_directory_uri() .'/includes/images/phone.png',
	    'menu_position' => null,
	    'supports' => array('title','editor','thumbnail'/*'author','excerpt','comments'*/),
		'taxonomies' => array('post_tag') // add tags so portfolio can be filtered
	  ); 
	  register_post_type('portfolio',$args);
	
	}
}

/*-----------------------------------------------------------------------------------*/
/* Custom Post Type - Feedback */
/*-----------------------------------------------------------------------------------*/

add_action('init', 'gomy_add_feedback');
if ( !function_exists('gomy_add_feedback') ) {
	function gomy_add_feedback() 
	{
	  $labels = array(
	    'name' => _x('Feedback', 'post type general name', 'gomysites'),
	    'singular_name' => _x('Feedback Item', 'post type singular name', 'gomysites'),
	    'add_new' => _x('Add New', 'slide', 'gomysites'),
	    'add_new_item' => __('Add New Feedback Item', 'gomysites'),
	    'edit_item' => __('Edit Feedback Item', 'gomysites'),
	    'new_item' => __('New Feedback Item', 'gomysites'),
	    'view_item' => __('View Feedback Item', 'gomysites'),
	    'search_items' => __('Search Feedback Items', 'gomysites'),
	    'not_found' =>  __('No Feedback Items found', 'gomysites'),
	    'not_found_in_trash' => __('No Feedback Items found in Trash', 'gomysites'), 
	    'parent_item_colon' => ''
	  );
	  $args = array(
	    'labels' => $labels,
	    'public' => false,
	    'publicly_queryable' => true,
		'_builtin' => false,
	    'show_ui' => true, 
	    'query_var' => true,
	    'rewrite' => true,
	    'capability_type' => 'post',
	    'hierarchical' => false,
	    'menu_icon' => get_template_directory_uri() .'/includes/images/feedback.png',
	    'menu_position' => null,
	    'supports' => array('title','editor',/*'author','thumbnail','excerpt','comments'*/),
	  ); 
	  register_post_type('feedback',$args);
	
	}
}

/*-----------------------------------------------------------------------------------*/
/* Slider Button Shortcode */
/*-----------------------------------------------------------------------------------*/

function slider_button($atts, $content = null) {
   extract(shortcode_atts(array('url' => '#'), $atts));
   return '<a class="btn" href="'.$url.'"><span>' . do_shortcode($content) . '</span></a>';
}
add_shortcode('slidebutton', 'slider_button');

/*-----------------------------------------------------------------------------------*/
/* Subscribe / Connect */
/*-----------------------------------------------------------------------------------*/

if (!function_exists('gomy_subscribe_connect')) {
	function gomy_subscribe_connect($widget = 'false', $title = '', $form = '', $social = '') {

		global $gomy_options;

		// Setup title
		if ( $widget != 'true' )
			$title = $gomy_options[ 'gomy_connect_title' ];

		// Setup related post (not in widget)
		$related_posts = '';
		if ( $gomy_options[ 'gomy_connect_related' ] == "true" AND $widget != "true" )
			$related_posts = do_shortcode('[related_posts limit="5"]');

?>
	<?php if ( $gomy_options[ 'gomy_connect' ] == "true" OR $widget == 'true' ) : ?>
	<div id="connect">
		<h3><?php if ( $title ) echo $title; else _e('Subscribe','gomysites'); ?></h3>

		<div <?php if ( $related_posts != '' ) echo 'class="col-left"'; ?>>
			<p><?php if ($gomy_options[ 'gomy_connect_content' ] != '') echo stripslashes($gomy_options[ 'gomy_connect_content' ]); else _e('Subscribe to our e-mail newsletter to receive updates.', 'gomysites'); ?></p>

			<?php if ( $gomy_options[ 'gomy_connect_newsletter_id' ] != "" AND $form != 'on' ) : ?>
			<form class="newsletter-form<?php if ( $related_posts == '' ) echo ' fl'; ?>" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $gomy_options[ 'gomy_connect_newsletter_id' ]; ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
				<input class="email" type="text" name="email" value="<?php _e('Enter your email address','gomysites'); ?>" onfocus="if (this.value == '<?php _e('Enter your email address','gomysites'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('Enter your email address','gomysites'); ?>';}" />
				<input type="hidden" value="<?php echo $gomy_options[ 'gomy_connect_newsletter_id' ]; ?>" name="uri"/>
				<input type="hidden" value="<?php bloginfo('name'); ?>" name="title"/>
				<input type="hidden" name="loc" value="en_US"/>
				<input class="submit" type="submit" name="submit" value="<?php _e('Subscribe', 'gomysites'); ?>" />
			</form>
			<?php endif; ?>

			<?php if ( $gomy_options['gomy_connect_mailchimp_list_url'] != "" AND $form != 'on' AND $gomy_options['gomy_connect_newsletter_id'] == "" ) : ?>
			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
				<form class="newsletter-form<?php if ( $related_posts == '' ) echo ' fl'; ?>" action="<?php echo $gomy_options['gomy_connect_mailchimp_list_url']; ?>" method="post" target="popupwindow" onsubmit="window.open('<?php echo $gomy_options['gomy_connect_mailchimp_list_url']; ?>', 'popupwindow', 'scrollbars=yes,width=650,height=520');return true">
					<input type="text" name="EMAIL" class="required email" value="<?php _e('E-mail','gomysites'); ?>"  id="mce-EMAIL" onfocus="if (this.value == '<?php _e('E-mail','gomysites'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e('E-mail','gomysites'); ?>';}">
					<input type="submit" value="<?php _e('Submit', 'gomysites'); ?>" name="subscribe" id="mc-embedded-subscribe" class="btn submit button">
				</form>
			</div>
			<!--End mc_embed_signup-->
			<?php endif; ?>

			<?php if ( $social != 'on' ) : ?>
			<div class="social<?php if ( $related_posts == '' AND $gomy_options[ 'gomy_connect_newsletter_id' ] != "" ) echo ' fr'; ?>">
		   		<?php if ( $gomy_options[ 'gomy_connect_rss' ] == "true" ) { ?>
		   		<a href="<?php if ( $gomy_options[ 'gomy_feed_url' ] ) { echo $gomy_options[ 'gomy_feed_url' ]; } else { echo get_bloginfo_rss('rss2_url'); } ?>" class="subscribe"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-rss.png" title="<?php _e('Subscribe to our RSS feed', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_twitter' ] != "" ) { ?>
		   		<a href="<?php echo $gomy_options[ 'gomy_connect_twitter' ]; ?>" class="twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-twitter.png" title="<?php _e('Follow us on Twitter', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_facebook' ] != "" ) { ?>
		   		<a href="<?php echo $gomy_options[ 'gomy_connect_facebook' ]; ?>" class="facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-facebook.png" title="<?php _e('Connect on Facebook', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_youtube' ] != "" ) { ?>
		   		<a href="<?php echo $gomy_options[ 'gomy_connect_youtube' ]; ?>" class="youtube"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-youtube.png" title="<?php _e('Watch on YouTube', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_flickr' ] != "" ) { ?>
		   		<a href="<?php echo $gomy_options[ 'gomy_connect_flickr' ]; ?>" class="flickr"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-flickr.png" title="<?php _e('See photos on Flickr', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_linkedin' ] != "" ) { ?>
		   		<a href="<?php echo $gomy_options[ 'gomy_connect_linkedin' ]; ?>" class="linkedin"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-linkedin.png" title="<?php _e('Connect on LinkedIn', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_delicious' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $gomy_options['gomy_connect_delicious'] ); ?>" class="delicious"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-delicious.png" title="<?php _e('Discover on Delicious', 'gomysites'); ?>" alt=""/></a>

		   		<?php } if ( $gomy_options[ 'gomy_connect_googleplus' ] != "" ) { ?>
		   		<a href="<?php echo esc_url( $gomy_options['gomy_connect_googleplus'] ); ?>" class="googleplus"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-googleplus.png" title="<?php _e('View Google+ profile', 'gomysites'); ?>" alt=""/></a>

				<?php } ?>
			</div>
			<?php endif; ?>

		</div><!-- col-left -->

		<?php if ( $gomy_options[ 'gomy_connect_related' ] == "true" AND $related_posts != '' ) : ?>
		<div class="related-posts col-right">
			<h4><?php _e('Related Posts:', 'gomysites'); ?></h4>
			<?php echo $related_posts; ?>
		</div><!-- col-right -->
		<?php wp_reset_query(); endif; ?>

        <div class="fix"></div>
	</div>
	<?php endif; ?>
<?php
	}
}


/*-----------------------------------------------------------------------------------*/
/* Exclude Pages */
/*-----------------------------------------------------------------------------------*/

function gomy_exclude_pages() {
	$exclude = '';	
	return $exclude;
}

/*-----------------------------------------------------------------------------------*/
/* Get Post image attachments */
/*-----------------------------------------------------------------------------------*/
/* 
Description:

This function will get all the attached post images that have been uploaded via the 
WP post image upload and return them in an array. 

*/
function gomy_get_post_images($offset = 1) {
	
	// Arguments
	$repeat = 100; 				// Number of maximum attachments to get 
	$photo_size = 'large';		// The WP "size" to use for the large image

	if ( !is_array($args) ) 
		parse_str( $args, $args );
	extract($args);

	global $post;

	$id = get_the_id();
	$attachments = get_children( array(
	'post_parent' => $id,
	'numberposts' => $repeat,
	'post_type' => 'attachment',
	'post_mime_type' => 'image',
	'order' => 'ASC', 
	'orderby' => 'menu_order date')
	);
	if ( !empty($attachments) ) :
		$output = array();
		$count = 0;
		foreach ( $attachments as $att_id => $attachment ) {
			$count++;  
			if ($count <= $offset) continue;
			$url = wp_get_attachment_image_src($att_id, $photo_size, true);	
			if ( $url[0] != $exclude )
				$output[] = array( "url" => $url[0], "caption" => $attachment->post_excerpt );
		}  
	endif; 
	return $output;
}


/*-----------------------------------------------------------------------------------*/
/* Remove Meta From Array (Portfolio Single) */
/*-----------------------------------------------------------------------------------*/

	/* gomy_remove_meta_from_array()
	 *
	 * Checks for data from each of the $meta_fields as removes
	 * it from the $array.
	 *
	 * Params:
	 * Array $array (required)
	 * Array $meta_fields (required)
	------------------------------------------------------------*/
	
	function gomy_remove_meta_from_array ( $array, $meta_fields ) {
	
		global $post;
	
		// Make sure we've got the right data types.
		foreach ( array( $array, $meta_fields ) as $a ) {
		
			if ( ! is_array( $a ) ) { return; } // End IF Statement
		
		} // End FOREACH Loop
	
		// This is the custom code where we strip out all the images that are listed in custom meta fields.
		// 2011-01-05.
		
		$image_meta_fields = $meta_fields;
		
		$non_gallery_images = array();
			
		// If "upload" custom fields exist, check them for data on the current entry.
		
		if ( count( $image_meta_fields ) ) {
		
			foreach ( $image_meta_fields as $im ) {
			
				$_value = get_post_meta( $post->ID, $im, true );
				
				if ( $_value ) {
				
					$non_gallery_images[] = $_value;
				
				} // End IF Statement
			
			} // End FOREACH Loop
		
		} // End IF Statement
		
		// If we have non-gallery images and attachments, begin our custom processing.
		
		if ( count( $non_gallery_images ) && count( $array ) ) {
		
			foreach ( $array as $k => $v ) {
			
				if ( in_array( $v->guid, $non_gallery_images ) ) {
				
					unset( $array[$k] );
				
				} // End IF Statement
			
			} // End FOREACH Loop
		
		} // End IF Statement
		
		return $array;
	
	} // End gomy_remove_meta_from_array()


add_filter( 'pre_get_posts', 'gomy_show_portfolio_in_tag' );


/*----------------------------------------
 gomy_show_portfolio_in_tag()
 ----------------------------------------
 
 * Make sure `portfolio` posts display
 * in `post_tag` archives as well as
 * the default post types.
----------------------------------------*/

function gomy_show_portfolio_in_tag ( $query ) {

   if ( $query->is_tag ) { $query->set( 'post_type', array( 'post', 'portfolio' ) ); }
   
   return $query;

} // End gomy_show_portfolio_in_tag()


/*-----------------------------------------------------------------------------------*/
/* MISC */
/*-----------------------------------------------------------------------------------*/



?>