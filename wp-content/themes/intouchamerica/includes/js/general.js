jQuery(document).ready(function(){

/*-----------------------------------------------------------------------------------*/
/* Portfolio Carousel (jCarouselLite) */
/*-----------------------------------------------------------------------------------*/

if ( jQuery( '.portfolio-carousel' ).length ) {
	if ( jQuery( '.portfolio-carousel .slides li' ).length > 1 ) {
		
		var visibleSlides = 5;
		var autoSpeed = parseInt( gomy_slider_settings.auto );
		var slideSpeed = parseInt( gomy_slider_settings.speed );
		
		// Make sure the slider doesn't freak out if there are fewer than 5 items.
		if ( jQuery( '.portfolio-carousel .slides li' ).length < 5 ) {
			visibleSlides = jQuery( '.portfolio-carousel .slides li' ).length;
		}
	
		jQuery( '.portfolio-carousel' ).jCarouselLite({
		
			btnNext: '#carousel-inner .btn-next', 
			btnPrev: '#carousel-inner .btn-prev', 
			auto: autoSpeed, 
			speed: slideSpeed, 
			visible: visibleSlides, 
			circular: false
		
		});
		
		jQuery( '#carousel-inner .btn-prev' ).addClass( 'disabled' );
		jQuery( '#carousel-inner .btn-next' ).click( function () {
			jQuery( '#carousel-inner .btn-prev.disabled' ).removeClass( 'disabled' );
		});

	}
}


/*-----------------------------------------------------------------------------------*/
/* Alert on CC pay enroll checkbox click */
/*-----------------------------------------------------------------------------------*/
$('#choice_12_1:checkbox').click(function(){
    alert('Thank you for enrolling in auto-pay. An InTouch America representative will contact you to confirm and finalize your request.')
});


/*-----------------------------------------------------------------------------------*/
/* Add alt-row styling to tables */
/*-----------------------------------------------------------------------------------*/

	jQuery( '.entry table tr:odd').addClass( 'alt-table-row' );


/*-----------------------------------------------------------------------------------*/
/*	Font Sizer
/*-----------------------------------------------------------------------------------*/
$(function(){
	$('p, h1, h2, h3, h4, h5, h6, .resize, .col1 ul li, .col2 ul li, .feature ul li').jfontsize({
	    btnMinusClasseId: '#jfontsize-m',
	    btnDefaultClasseId: '#jfontsize-d',
	    btnPlusClasseId: '#jfontsize-p',
	    btnMinusMaxHits: 5,
	    btnPlusMaxHits: 8,
	    sizeChange: 2
	});
});

// This stops a font sizer links highlighting on doubleclick
$(function(){
    $.extend($.fn.disableTextSelect = function() {
        return this.each(function(){
            if($.browser.mozilla){//Firefox
                $(this).css('MozUserSelect','none');
            }else if($.browser.msie){//IE
                $(this).bind('selectstart',function(){return false;});
            }else{//Opera, etc.
                $(this).mousedown(function(){return false;});
            }
        });
    });
    $('li.size a').disableTextSelect();
});


/*-----------------------------------------------------------------------------------*/
/* Superfish navigation dropdown */
/*-----------------------------------------------------------------------------------*/

if(jQuery().superfish) {
		jQuery( 'ul.nav').superfish({
			delay: 200,
			animation: {opacity:'show', height:'show'},
			speed: 'fast',
			dropShadows: false
		});
}


});
