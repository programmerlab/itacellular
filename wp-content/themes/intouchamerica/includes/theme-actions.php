<?php 

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Add custom styling to HEAD
- Add custom typograhpy to HEAD
- Add layout to body_class output
- Featured Slider Settings
- Single Portfolio Gallery Settings

-----------------------------------------------------------------------------------*/


add_filter('body_class','gomy_layout_body_class');		// Add layout to body_class output


/*-----------------------------------------------------------------------------------*/
/* Add layout to body_class output */
/*-----------------------------------------------------------------------------------*/
if (!function_exists('gomy_layout_body_class')) {
	function gomy_layout_body_class($classes) {
		
		global $gomy_options;
		$layout = $gomy_options[ 'gomy_site_layout' ];

		// Set main layout on post or page
		if ( is_singular() ) {
			global $post;
			$single = get_post_meta($post->ID, '_layout', true);
			if ( $single != "" AND $single != "layout-default" ) 
				$layout = $single;
		}
		
		// Add layout to $gomy_options array for use in theme
		$gomy_options[ 'gomy_layout' ] = $layout;
		
		// Add classes to body_class() output 
		$classes[] = $layout;
		return $classes;						
					
	}
}

/*-----------------------------------------------------------------------------------*/
/* Featured Slider Settings */
/*-----------------------------------------------------------------------------------*/

add_filter('gomy_head', 'gomy_slider_options');
if (!function_exists('gomy_slider_options')) {
function gomy_slider_options() { 
	
	global $gomy_options;
	
	if ($gomy_options[ 'gomy_slider' ] == 'true' && is_home() && !is_paged()): ?>
	
		<script type="text/javascript">
		<!--//--><![CDATA[//><!--
			jQuery(window).load(function(){
						
				jQuery('#slides').slides({
					start: 1,
					<?php if ($gomy_options[ 'gomy_slider_autoheight' ] == "true"): ?>
					autoHeight: true,
					<?php endif; ?>
					effect: '<?php echo $gomy_options[ 'gomy_slider_effect' ]; ?>',
					container: 'slides_container',
					<?php if ($gomy_options[ 'gomy_slider_random' ] == "true"): ?>			
					randomize: true,
					<?php endif; ?>
					<?php if ($gomy_options[ 'gomy_slider_hover' ] == "true"): ?>			
					hoverPause: true,
					<?php endif; ?>
					<?php if ($gomy_options[ 'gomy_slider_auto' ] == "true"): ?>
					play: <?php echo $gomy_options[ 'gomy_slider_interval' ] * 1000; ?>,
					<?php endif; ?>			
					slideSpeed: <?php echo $gomy_options[ 'gomy_slider_speed' ] * 1000; ?>,
					<?php if ($gomy_options[ 'gomy_slider_nextprev' ] == "true"): ?>
					generateNextPrev: true,
					<?php endif; ?>
					<?php if ($gomy_options[ 'gomy_slider_pagination' ] == "false"): ?>
					generatePagination: false,
					<?php endif; ?>
					crossfade: true
				});
				
				jQuery( '#slides .pagination' ).wrap( '<div id="slider_pag" />' );
				jQuery( '#slides #slider_pag' ).wrap( '<div id="slider_nav" />' );
				
			});
		//-->!]]>
		</script>
				
	<?php endif;

}
}

/*-----------------------------------------------------------------------------------*/
/* Single Portfolio Gallery Settings */
/*-----------------------------------------------------------------------------------*/

add_filter('gomy_head', 'gomy_portfolio_options');
if (!function_exists('gomy_portfolio_options')) {
	function gomy_portfolio_options() { 
	if ( is_singular( 'portfolio' ) ) {
	global $gomy_options;  ?>
		<script type="text/javascript">
			jQuery(window).load(function(){
			jQuery("#loopedSlider").loopedSlider({
				<?php
					$autoStart = 0;
					$slidespeed = 600;
					if ( get_option("gomy_single_auto") == "true" ) 
		   				$autoStart = get_option("gomy_single_interval") * 1000;
					else 
		   				$autoStart = 0;
					if ( get_option("gomy_single_speed") <> "" ) 
						$slidespeed = get_option("gomy_single_speed") * 1000;
				?>
				autoStart: <?php echo $autoStart; ?>, 
				slidespeed: <?php echo $slidespeed; ?>, 
				autoHeight: true
				});
			});
			
			/*-----------------------------------------------------------------------------------*/
			/* Single Portfolio Gallery */
			/*-----------------------------------------------------------------------------------*/
			
			jQuery(document).ready(function() {
			
				var show_thumbs = 3;
				
			    jQuery('#loopedSlider.gallery ul.pagination').jcarousel({
			    	visible: show_thumbs,
			    	wrap: 'both'
			    });
			});

		</script>
	<?php }}
}

/*-----------------------------------------------------------------------------------*/
/* END */
/*-----------------------------------------------------------------------------------*/
?>