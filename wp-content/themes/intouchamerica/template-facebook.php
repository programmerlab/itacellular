<?php
/*
Template Name: Facebook
*/
?>

<?php get_header(); ?>
       
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h1 class="title"><?php the_title(); ?></h1>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div><!-- end title section -->

</div><!-- end top section -->

	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?> 


    <div id="content" class="page facebook">

	    <div id="inner" class="col-full">
			           		
			<div id="main" class="fullwidth">
				
	            <?php if (have_posts()) : $count = 0; ?>
	            <?php while (have_posts()) : the_post(); $count++; ?>

	            
	            <?php the_content(); ?>   	

				
				<?php edit_post_link( __( '{ Edit }', 'gomysites' ), '<span class="small">', '</span>' ); ?>
	
	                                                    
				<?php endwhile; else: ?>
					<div <?php post_class(); ?>>
	                	<p><?php _e( 'Sorry, no posts matched your criteria.', 'gomysites' ) ?></p>
	                </div><!-- /.post -->
	            <?php endif; ?>  
	        
			</div><!-- /#main -->
			
		</div><!-- /#inner -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>