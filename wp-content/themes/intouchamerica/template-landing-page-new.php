<?php
/*
Template Name: New Landing Page
*/
?>

<?php get_header(); ?>

</div><!-- end top section -->


    <div id="content" class="page">
		
        <?php if (have_posts()) : $count = 0; ?>
        <?php while (have_posts()) : the_post(); $count++; ?>

        
        <?php the_content(); ?>   	

		
		<?php edit_post_link( __( '{ Edit }', 'gomysites' ), '<span class="small">', '</span>' ); ?>

                                                
		<?php endwhile; else: ?>
			<div <?php post_class(); ?>>
            	<p><?php _e( 'Sorry, no posts matched your criteria.', 'gomy' ) ?></p>
            </div><!-- /.post -->
        <?php endif; ?>  
			
    </div><!-- /#content -->
		
		
<?php get_footer(); ?>