<?php
/*
Template Name: Seniors
*/
?>

<?php get_header(); ?>
       
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h1 class="title"><?php the_title(); ?></h1>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div><!-- end title section -->

</div><!-- end top section -->

	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?> 


    <div id="content" class="page facebook">

	    <div id="inner" class="col-full">
			           		
			<div id="main" class="fullwidth">
			
				<div class="fbheading">
					<h1>PEACE OF MIND FOR PENNIES A DAY!</h1>
					
					<div class="fix"></div>
				</div>
				
				
				<div class="fb-panel">
					<p>Established in 1988, InTouch America is a super affordable cellular phone company that offers you the best wireless networks in the United States for as little as 25 cents per day! We bring peace of mind and security to our seniors and their families by knowing help is always just a phone call away.</p>
				
					<img src="<?php bloginfo('template_directory'); ?>/images/fb-samsung-u310.jpg" alt="samsung u310" class="fb-samsung" />
					
					<h3 style="margin-top:25px;">Choose from 5 Great Plans:</h3>
					<ul>
						<li><span>$4.99 per month, Includes 10 Free Minutes - Only 15 cents for additional minutes.</span></li>
						<li><span>$7.99 per month, Includes 50 Free Minutes - Only 25 cents for additional minutes.</span></li>
						<li><span>$9.99 per month, Includes 100 Free Minutes - Only 25 cents for additional minutes.</span></li>
						<li><span>$19.99 per month, Includes 300 Free Minutes - Only 25 cents for additional minutes.</span></li>
						<li><span>$29.99 per month, Includes 700 Free Minutes - Only 25 cents for additional minutes.</span></li>
					</ul>
					
					
					<h3>Top it off with:</h3>
					<ul>
						<li><span>3 months of Free Service on any of the above plan (up to a $90 value).</span></li>
						<li><span>Free Samsung Knack Easy to use cellular phone ($150 value).</span></li>
						<li><span>Call or travel anywhere in the United States for Free since minutes are national.</span></li>
						<li><span>Free Activation on any $7.99 plan or higher ($36 value).</span></li>
					</ul>
					
					
					<h3>Just some Extra Reasons to Sign Up:</h3>
					<ul>
						<li><span>Need more minutes? We custom tailor packages to meet every need.</span></li>
						<li><span>Why pay up to $50/month for medical alarms or high cell phone bills?</span></li>
						<li><span>Have a cell phone already? Keep your same number and switch to us.</span></li>
						<li><span>NO-CONTRACT, month to month options available on all plans.</span></li>
						<li><span>US Based Customer Service.</span></li>
					</ul>
					
					<div class="fix"></div>
				</div>
				<div class="fb-panel-btm"></div>
				
				<div class="fb-like-panel">
					<h2 class="fb-like-text"><a href="http://www.facebook.com/pages/InTouch-America/181833780250">Like us on Facebook for your chance to win a brand new 16 GB Apple iPhone 4s.</a></h2>
					<div class="fb-like"><a href="http://www.facebook.com/pages/InTouch-America/181833780250"><img src="http://www.intouchamerica.com/wp-content/themes/intouchamerica/images/ico-facebook.png" alt="like us on facebook" title="like us on facebook" /></a></div>
					<div class="fix"></div>
				</div>
	
				<h2><span class="call">Call us at:</span> <span class="number">(800) 500-0066</span> <br /> <span class="details">for details or fill in the form below <br /> Open Monday through Friday</span></h2>
				
	            <?php if (have_posts()) : $count = 0; ?>
	            <?php while (have_posts()) : the_post(); $count++; ?>
	                                                                        
	                
                    <div class="fb-email-panel">
	                	<?php the_content(); ?>
	               	</div>
	               	<div class="shadow"></div>
	               	

					<?php edit_post_link( __( '{ Edit }', 'gomysites' ), '<span class="small">', '</span>' ); ?>
	
	                                                    
				<?php endwhile; else: ?>
					<div <?php post_class(); ?>>
	                	<p><?php _e( 'Sorry, no posts matched your criteria.', 'gomysites' ) ?></p>
	                </div><!-- /.post -->
	            <?php endif; ?>  
	        
			</div><!-- /#main -->
			
		</div><!-- /#inner -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>