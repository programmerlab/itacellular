<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title(''); ?></title>
<?php gomy_meta(); ?>
<?php global $gomy_options; ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( $gomy_options[ 'gomy_feed_url' ] ) { echo $gomy_options[ 'gomy_feed_url' ]; } else { echo get_bloginfo_rss('rss2_url'); } ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<script type="text/javascript" src="http://use.typekit.com/klt5wbb.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<link rel="shortcut icon" href="/wp-content/themes/intouchamerica/images/favicon.ico" >
      
<?php wp_head(); ?>
<?php gomy_head(); ?> 

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "979de50b-efd8-4e0a-b997-bb7afa8dd5a3", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script type="text/javascript">stLight.options({publisher: "979de50b-efd8-4e0a-b997-bb7afa8dd5a3", hashAddressBar:false});</script>

<!-- Facebook Conversion Code for faecbook - go ad -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6013604685378', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6013604685378&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
_fbq.push(['addPixelId', '829034050450050']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=829034050450050&amp;ev=PixelInitialized" /></noscript>

</head>

<body <?php body_class(); ?>>
<?php gomy_top(); ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NHHCMF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NHHCMF');</script>
<!-- End Google Tag Manager -->

<div id="wrapper"><!-- start wrapper -->
	
	<div id="top-section"><!-- start top section -->

		<?php if ( function_exists( 'has_nav_menu') && has_nav_menu( 'top-menu') ) { ?>
		
		<div id="top"><!-- start top nav -->
			<div class="col-full">
				<?php wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'top-nav', 'menu_class' => 'nav fl', 'theme_location' => 'top-menu' ) ); ?>
			</div>
		</div><!-- end top nav -->
		
	    <?php } ?>
    
    
	    <div id="header-top"><!-- start top header -->
	    	<div class="col-full">
	    		<h2>Reliable Wireless Solutions at Affordable Prices</h2>
	    		
	    		<h3>(800) 500-0066</h3>
	    		
	    		<a href="https://www.intouchamerica.com/online-payment/" class="payment">Make a Payment</a>
	    		
				<div class="text-resizer"><!-- start .text-resizer -->
					<ul>
						<li class="resizer">Text Size:</li>
						<li class="size"><a id="jfontsize-m" class="jfontsize-button" style="cursor:pointer;">A-</a></li>
						<li class="size"><a id="jfontsize-d" class="jfontsize-button" style="cursor:pointer;">A</a></li>
						<li class="size"><a id="jfontsize-p" class="jfontsize-button" style="cursor:pointer;">A+</a></li>
					</ul>
					<div class="clear"></div>
				</div><!-- end .text-resizer -->
				
				<div id="language-switcher"><!--start language -->
					<?php gomy_sidebar('sidebar-header'); ?>		
				</div><!--end language -->
				
	    	</div>
	    </div><!-- end top header -->
	   
           
           
		<div id="header-container"><!-- start header container -->      
			<div id="header" class="col-full"><!-- start header -->
	 		       
				<div id="logo"><!-- start logo -->
					<?php if ($gomy_options[ 'gomy_texttitle' ] <> "true") : $logo = $gomy_options[ 'gomy_logo' ]; ?>
						<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('description'); ?>">
							<img src="<?php if ($logo) echo $logo; else { bloginfo('template_directory'); ?>/images/logo.png<?php } ?>" alt="<?php bloginfo('name'); ?>" />
						</a>
			        <?php endif; ?> 
			        
			        <?php if( is_singular() && !is_front_page() ) : ?>
						<span class="site-title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></span>
			        <?php else : ?>
						<h1 class="site-title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
			        <?php endif; ?>
						<span class="site-description"><?php bloginfo('description'); ?></span>
				</div><!-- end logo -->
			       
			       
				<div id="navigation" class="fr"><!-- start navigation -->
		
					<?php
					if ( function_exists('has_nav_menu') && has_nav_menu('primary-menu') ) {
						wp_nav_menu( array( 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'main-nav', 'menu_class' => 'nav fl', 'theme_location' => 'primary-menu' ) );
					} else {
					?>
					<ul id="main-nav" class="nav fl">
						<?php 
						if ( get_option('gomy_custom_nav_menu') == 'true' ) {
							if ( function_exists('gomy_custom_navigation_output') )
								gomy_custom_navigation_output("name=gomy Menu 1");
				
						} else { ?>
							
							<?php if ( is_page() ) $highlight = "page_item"; else $highlight = "page_item current_page_item"; ?>
							<li class="<?php echo $highlight; ?>"><a href="<?php bloginfo('url'); ?>"><?php _e('Home', 'gomysites') ?></a></li>
							<?php wp_list_pages('sort_column=menu_order&depth=6&title_li=&exclude='); ?>
				
						<?php } ?>
					</ul><!-- /#nav -->
					<?php } ?>
					<?php if ( $gomy_options['gomy_nav_rss'] == "true" ) { ?>
					<ul class="rss fr">
						<li class="sub-rss"><a href="<?php if ( $gomy_options['gomy_feed_url'] ) { echo $gomy_options['gomy_feed_url']; } else { echo get_bloginfo_rss('rss2_url'); } ?>"><img src="<?php echo bloginfo('template_directory'); ?>/images/ico-rss.png" alt="<?php bloginfo('name'); ?>" /></a></li>
					</ul>
					<?php } ?>
					
				</div><!-- end navigation -->
				
			</div><!-- end header -->
		</div><!-- end header-container -->

