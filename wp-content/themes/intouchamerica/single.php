<?php get_header(); ?>
<?php global $gomy_options; ?>

    <?php 
    $blog_title = __('The Blog', 'gomysites');
    if ( is_array( $gomy_options ) && @$gomy_options['gomy_blog_title'] ) {
						$blog_title = $gomy_options[ 'gomy_blog_title' ];
	}
 	if ( is_array( $gomy_options ) && @$gomy_options['gomy_blog_description'] ) { 
						$blog_description = $gomy_options[ 'gomy_blog_description' ];
	 }
	?>   
					
	<div id="title-container" class="col-full post"><!-- start title section -->
		<h2 class="title"><?php echo $blog_title ?></h2>
		<?php if ( $blog_description )  { ?>
		<span class="blog-title-sep">&bull;</span><span class="description"><?php echo $blog_description ?></span>
		<?php } ?>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div><!-- end title section -->
	
	
</div><!-- end top section -->	


	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
			
			<div id="post-entries">
	            <div class="nav-prev fl"><?php previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title' ) ?></div>
	            <div class="nav-next fr"><?php next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' ) ?></div>
	            <div class="fix"></div>
	        </div><!-- #post-entries -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?>
	
	
<div id="main-content"><!-- start main content area -->

    <div class="col-full"><!--start content-->
 	
		<div id="main" class="col-left"><!--start main-->
	    
	        <?php if (have_posts()) : $count = 0; ?>
	        <?php while (have_posts()) : the_post(); $count++; ?>
	        
				<div <?php post_class(); ?>>
				
	                <h1 class="title"><?php the_title(); ?></h1>
	                                
	                <?php gomy_post_meta(); ?>
	
					<?php echo gomy_embed( 'width=580' ); ?>
	                <?php if ( $gomy_options[ 'gomy_thumb_single' ] == "true" && !gomy_embed( '')) gomy_image( 'width='.$gomy_options[ 'gomy_single_w' ].'&height='.$gomy_options[ 'gomy_single_h' ].'&class=thumbnail '.$gomy_options[ 'gomy_thumb_single_align' ]); ?>
	                                
	                <div class="entry">
	                	<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'gomysites' ), 'after' => '</div>' ) ); ?>
					</div>
										
					<?php the_tags( '<p class="tags">'.__( 'Tags: ', 'gomysites' ), ', ', '</p>' ); ?>
					
					<div class="social-section"><!-- Start .social-section -->
						<h4>Share this blog post:</h4>
						
						<div class="icons">
							<span class='st_facebook_large' displayText='Facebook'></span>
							<span class='st_twitter_large' displayText='Tweet'></span>
							<span class='st_pinterest_large' displayText='Pinterest'></span>
							<span class='st_email_large' displayText='Email'></span>
							<span class='st_instagram_large' displayText='Instagram Badge' st_username='intouchamerica'></span>
							<span class='st_sharethis_large' displayText='ShareThis'></span>
						</div>
						<div class="fix"></div>
					</div><!-- End .social-section -->
	                                
	            </div><!-- .post -->
	
					<?php if ( $gomy_options[ 'gomy_post_author' ] == "true" ) { ?>
					<div id="post-author">
						<div class="profile-image"><?php echo get_avatar( get_the_author_meta( 'ID' ), '70' ); ?></div>
						<div class="profile-content">
							<h3 class="title"><?php printf( esc_attr__( 'About %s', 'gomysites' ), get_the_author() ); ?></h3>
							<?php the_author_meta( 'description' ); ?>
							<div class="profile-link">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
									<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'gomysites' ), get_the_author() ); ?>
								</a>
							</div><!-- #profile-link	-->
						</div><!-- .post-entries -->
						<div class="fix"></div>
					</div><!-- #post-author -->
					<?php } ?>
	
					<?php gomy_subscribe_connect(); ?>
	
		        <?php $comm = $gomy_options[ 'gomy_comments' ]; if ( ($comm == "post" || $comm == "both") ) : ?>
	                <?php comments_template( '', true); ?>
	            <?php endif; ?>
	                                                
			<?php endwhile; else: ?>
				<div <?php post_class(); ?>>
	            	<p><?php _e( 'Sorry, no posts matched your criteria.', 'gomysites' ) ?></p>
				</div><!-- .post -->             
	       	<?php endif; ?>  
	        
			</div><!--end main-->
	
	        <?php get_sidebar(); ?>

    </div><!--end content-->
    
</div><!-- end main content area -->
		
<?php get_footer(); ?>