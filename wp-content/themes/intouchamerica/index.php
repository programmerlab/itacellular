<?php get_header(); ?>
<?php global $gomy_options; 

	// Determine whether or not the homepage sidebar is enabled (enabled by default).
	// Also determine the various differences in logic if the sidebar is enabled/disabled.
	$has_sidebar = true;
	$content_css_class = ' home-sidebar';
	$main_css_class = 'col-left';
	$mini_features_count = 2;
	
	if ( is_array( $gomy_options ) && @$gomy_options['gomy_home_sidebar'] == 'true' ) {
	$has_sidebar = false;
	$content_css_class = '';
	$main_css_class = 'col-full';
	$mini_features_count = 3;
	}
?>
	
	<?php if ( $gomy_options[ 'gomy_slider' ] == 'true' && is_home() ) { get_template_part( 'includes/featured' ); } ?>
	
</div><!-- end top section -->


	<div id="main-content"><!-- start content container -->
	    
	    <div id="six-reasons-section"> <!-- start col full -->
	    	<div class="col-full">
	    		    	
	    	<?php /* <div class="christmas-lights"></div> */ ?>
	    
		    <!-- start portfolio --> 
		    <?php
		       if ($gomy_options[ 'gomy_portfolio' ] == 'true' && ! is_paged() ) { ?>
		    		<div id="carousel-inner">
		    			<?php get_template_part( 'includes/portfolio', 'carousel' ); // Carousel of portfolio items. ?>
		    		</div>
		    		<div id="carousel-content-border"></div>
		    <?php } ?>
		    <!-- end portfolio -->
	    
	    
			<!-- <div id="main" class="<?php echo $main_css_class; ?>">--> <!-- start main content -->   
			
			
			<!-- <div class="xmas-lights"></div> -->
			
			
						   
	                   
	            <?php if ( $gomy_options['gomy_main_page1'] && $gomy_options['gomy_main_page1'] <> "Select a page:" && ! is_paged() ) { ?>
		        <div id="main-page1"><!-- start section 1 -->
					<?php query_posts('page_id=' . get_page_id($gomy_options['gomy_main_page1'])); ?>
		            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>		        					
				    <div class="entry"><?php the_content(); ?></div>
		            <?php endwhile; endif; ?>
		            <div class="fix"></div>
		        </div><!-- end section 1 -->
		        <?php } ?>
	               
	    	</div>
	    </div><!-- end col full -->
	 
	                    
        <?php if ($gomy_options['gomy_mini_features'] == "true" && ! is_paged() ): ?>
        <div id="mini-features"><!-- start mini features -->
        	<div class="col-full"><!-- start col full -->
        
		        <?php query_posts('post_type=infobox&order=DESC&posts_per_page=20'); ?>
		        <?php if (have_posts()) : $counter = 0; while (have_posts()) : the_post(); $counter++; ?>		        					
		
					<?php 
						$icon = get_post_meta($post->ID, 'mini', true); 
						$excerpt = stripslashes(get_post_meta($post->ID, 'mini_excerpt', true)); 
						$button = get_post_meta($post->ID, 'mini_readmore', true);
					?>
					<div class="block <?php if ($counter % $mini_features_count == 2) { echo ' last'; $counter = 2; } ?>">
						
						<h3><?php echo get_the_title(); ?></h3>
						
						<?php if ( $icon ) { ?>
		                <a href="<?php echo $button; ?>"><img src="<?php echo $icon; ?>" alt="<?php echo get_the_title(); ?>" class="home-icon" /></a>		
		                <?php } ?> 
		                                                     
		                <div class="<?php if ( $icon ) echo 'feature'; if ( $counter == 2 ) echo ' last'; ?>">
		                   
		                   <?php the_content(); ?>
		                   <?php /* if ( $button ) { ?><a href="<?php echo $button; ?>" class="button"><?php _e('Read More', 'gomysites'); ?></a><?php } */ ?>
		                </div>
					</div>
					<?php if ( $counter % $mini_features_count == 0 ) { echo '<div class="fix"></div>'; }  ?>				
		                
		        <?php endwhile; endif; ?>
		        
		        <div class="bbb-logo">
					<a class="erhzbul" href="http://www.bbb.org/losangelessiliconvalley/business-reviews/cellular-telephone-service-and-supplies/intouch-america-in-tarzana-ca-13067780#bbbseal" title="InTouch America, Inc., Cellular Telephone Service & Supplies, Tarzana, CA" style="display: block;position: relative;overflow: hidden; width: 293px; height: 61px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" src="http://seal-sanjose.bbb.org/logo/erhzbul/intouch-america-13067780.png" width="293" height="61" alt="InTouch America, Inc., Cellular Telephone Service & Supplies, Tarzana, CA" /></a>
				</div>
        
			</div><!-- end col full -->
        </div><!-- end mini features -->
        <?php endif; ?>	
	
	
		<div class="col-full"><!-- start col full -->
		
		        <?php if ( $gomy_options['gomy_main_page2'] && $gomy_options['gomy_main_page2'] <> "Select a page:" && ! is_paged() ) { ?>
		        <div id="main-page2"><!-- start section 2 -->
					<?php query_posts('page_id=' . get_page_id($gomy_options['gomy_main_page2'])); ?>
		            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>		        					
				    <div class="entry"><?php the_content(); ?></div>
		            <?php endwhile; endif; ?>
		            <div class="fix"></div>
		        </div><!-- end section 2 -->
		        <?php } ?>
	
	
		        <?php if ($gomy_options['gomy_latest'] == "true"): ?>
		        <div id="latest-blog-posts">
	
					<?php
						 $posts = $gomy_options['gomy_latest_entries'];
						 $enable_pagination = $gomy_options['gomy_latest_pagination'];
						 if ( $enable_pagination != 'true' ) { $enable_pagination = 'false'; }
						 
						 if ( ! isset( $paged ) ) { $paged = 1; }
						 
						 $args = array();
						 $args['posts_per_page'] = $posts;
						 $args['paged'] = 1;
						 
						 if ( $enable_pagination == 'true' ) {
						 
						 	$entries_per_page = $gomy_options['gomy_latest_entries_per_page'];
						 	if ( !isset( $entries_per_page ) ) { $entries_per_page = 5; }
						 	
							$args['posts_per_page'] = $entries_per_page;
							$args['paged'] = $paged;
						 
						 }
						 
						 query_posts( $args );
						 
						 if ( have_posts() ) : while ( have_posts() ) : the_post();
						 
						 $ico_cal = $gomy_options[ 'gomy_post_calendar' ] == "true";
						 $full_content = $gomy_options[ 'gomy_post_content' ] != "content";
					?>
		
				    <div <?php post_class(); ?>>
				    
					    <?php if ( $full_content ) { if ( $ico_cal ) { ?>
					    <div class="ico-cal alignleft"><div class="ico-day"><?php the_time('d'); ?></div><div class="ico-month"><?php the_time('M'); ?></div></div>
					    <?php } else { gomy_image( 'width='.$gomy_options[ 'gomy_thumb_w' ].'&height='.$gomy_options[ 'gomy_thumb_h' ].'&class=thumbnail '.$gomy_options[ 'gomy_thumb_align' ]); }} ?>
					    	
					    	<div class="details" <?php if ( $ico_cal && $full_content ) { echo 'style="margin-left:52px;"'; } ?>>
					    	
				        	<h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
				        	
				            <?php gomy_post_meta(); ?>
				            
				            <div class="entry">
			                    <?php if ( $gomy_options[ 'gomy_post_content' ] == "content" ) the_content(__( 'Read More...', 'gomysites' )); else the_excerpt(); ?>
			                </div><!-- /.entry -->
			                
		                <div class="post-more">      
		                	<?php if ( $gomy_options[ 'gomy_post_content' ] == "excerpt" ) { ?>
		                    <span class="read-more"><a href="<?php the_permalink() ?>" title="<?php esc_attr_e( 'Continue Reading &rarr;', 'gomysites' ); ?>"><?php _e( 'Continue Reading &rarr;', 'gomysites' ); ?></a></span>
		                    <?php } ?>
		                </div>
		
			           </div><!-- /.details -->
			        </div><!-- /.post -->
					    
					<?php
							endwhile;
								if ( $enable_pagination == 'true' ) { gomy_pagenav(); }
						endif;
					?>
					
				</div><!-- /#latest-blog-posts -->	
				<?php endif; ?>	        
	        
	            <div class="fix"></div>
	                                
			<!--</div> end main content -->

        	<?php if ( @$gomy_options['gomy_home_sidebar'] == 'false' ) { get_sidebar(); } ?>
        
        	
	    </div><!-- end col full -->
    </div><!-- end content container -->
 <script>
document.getElementsByClassName("payment")[0].href = "https://www.itacellular.com/online-payment/"
</script>
		
<?php get_footer(); ?>