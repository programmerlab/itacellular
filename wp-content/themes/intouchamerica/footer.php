<div id="footer-container"><!-- start footer wrapper -->

<?php global $gomy_options; ?>
	<?php 
		$total = 4;
		if ( isset( $gomy_options['gomy_footer_sidebars'] ) ) { $total = $gomy_options['gomy_footer_sidebars']; }
			   
		if ( ( gomy_active_sidebar( 'footer-1' ) ||
			   gomy_active_sidebar( 'footer-2' ) || 
			   gomy_active_sidebar( 'footer-3' ) || 
			   gomy_active_sidebar( 'footer-4' ) ) && $total > 0 ) : 
  	?>
	<div id="footer-widgets" class="col-full col-<?php echo $total; ?>"><!-- start footer widgets -->
		
		<?php $i = 0; while ( $i < $total ) : $i++; ?>			
			<?php if ( gomy_active_sidebar( 'footer-' . $i ) ) { ?>

		<div class="block footer-widget-<?php echo $i; ?>">
        	<?php gomy_sidebar( 'footer-' . $i ); ?>    
		</div>
		        
	        <?php } ?>
		<?php endwhile; ?>
        		        
		<div class="fix"></div>

	</div><!-- end footer-widgets -->
    <?php endif; ?>
    
	<div id="footer" class="col-full"><!-- start footer -->
	
		<div id="copyright" class="col-left">
		<?php if( isset( $gomy_options['gomy_footer_left'] ) && $gomy_options['gomy_footer_left'] == 'true' ) {
		
				echo stripslashes( do_shortcode( $gomy_options['gomy_footer_left_text'] ) );	

		} else { ?>
			<p><?php bloginfo(); ?> &copy; <?php echo date( 'Y' ); ?>. <?php _e( 'All Rights Reserved.', 'gomysites' ) ?></p>
		<?php } ?>
		</div>
		
		<div class="ft-social">
    		<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FInTouch-America-Wireless%2F181833780250&amp;send=false&amp;layout=standard&amp;width=55&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=lucida+grande&amp;height=35" scrolling="no" frameborder="0" style="border:none;overflow:hidden;width:55px;height:30px;float:left;margin:0 10px 0 0;" allowTransparency="true"></iframe>
    	</div>
    	
    	<div class="ft-social">
    		<a href="https://twitter.com/InTouchAmerica" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @InTouchAmerica</a>
    		<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    	</div>
    	
    	<div class="ft-social">
    		<a href="http://www.manta.com/c/mm7s0zj/intouch-america?referid=11069&rl=see-us-on-manta" target="_blank"><img src="http://www.manta.com/manta/images/mantaBadge_seeus.png" border="0" alt="mantra" /></a>
    	</div>
    	
    	<div class="ft-social">
	    	<a href="http://www.trustlink.org/Reviews/InTouch-America-205749386" target="_blank"><img src="http://www.intouchamerica.com/wp-content/themes/intouchamerica/images/ft-trustlink.png" border="0" alt="trustlink" /></a>
    	</div>
    	
    	<div class="ft-social">
	    	<a href="http://www.la.bbb.org/" target="_blank"><img src="http://www.intouchamerica.com/wp-content/themes/intouchamerica/images/ft-bbb.png" border="0" alt="bbb" /></a>
    	</div>
		
		<div id="credit" class="col-right">
        <?php if( isset( $gomy_options['gomy_footer_right'] ) && $gomy_options['gomy_footer_right'] == 'true' ){
		
        	echo stripslashes( do_shortcode( $gomy_options['gomy_footer_right_text'] ) );
       	
		} else { ?>
			<p><?php _e( 'Powered by', 'gomysites' ); ?> <a href="http://www.wordpress.org">WordPress</a>.</p>
		<?php } ?>
		</div>
		
	</div><!-- end footer -->
	
</div><!-- end footer wrapper -->


</div><!-- end wrapper -->
<?php wp_footer(); ?>
<?php gomy_foot(); ?>




</body>
</html>