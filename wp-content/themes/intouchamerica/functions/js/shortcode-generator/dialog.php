<?php 
// Get the path to the root.
$full_path = __FILE__;

$path_bits = explode( 'wp-content', $full_path );

$url = $path_bits[0];

// Require WordPress bootstrap.
require_once( $url . '/wp-load.php' );
                                   
$gomy_framework_version = get_option( 'gomy_framework_version' );

$MIN_VERSION = '2.9';

$meetsMinVersion = version_compare($gomy_framework_version, $MIN_VERSION) >= 0;

$gomy_framework_path = dirname(__FILE__) .  '/../../';

$gomy_framework_url = get_template_directory_uri() . '/functions/';

$gomy_shortcode_css = $gomy_framework_path . 'css/shortcodes.css';
                                  
$isgomyTheme = file_exists($gomy_shortcode_css);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<div id="gomy-dialog">

<?php if ( $meetsMinVersion && $isgomyTheme ) { ?>

<div id="gomy-options-buttons" class="clear">
	<div class="alignleft">
	
	    <input type="button" id="gomy-btn-cancel" class="button" name="cancel" value="Cancel" accesskey="C" />
	    
	</div>
	<div class="alignright">
	
	    <input type="button" id="gomy-btn-preview" class="button" name="preview" value="Preview" accesskey="P" />
	    <input type="button" id="gomy-btn-insert" class="button-primary" name="insert" value="Insert" accesskey="I" />
	    
	</div>
	<div class="clear"></div><!--/.clear-->
</div><!--/#gomy-options-buttons .clear-->

<div id="gomy-options" class="alignleft">
    <h3><?php echo __( 'Customize the Shortcode', 'gomysites' ); ?></h3>
    
	<table id="gomy-options-table">
	</table>

</div>

<div id="gomy-preview" class="alignleft">

    <h3><?php echo __( 'Preview', 'gomysites' ); ?></h3>

    <iframe id="gomy-preview-iframe" frameborder="0" style="width:100%;height:250px" scrolling="no"></iframe>   
    
</div>
<div class="clear"></div>


<script type="text/javascript" src="<?php echo $gomy_framework_url; ?>js/shortcode-generator/js/column-control.js"></script>
<script type="text/javascript" src="<?php echo $gomy_framework_url; ?>js/shortcode-generator/js/tab-control.js"></script>
<?php  }  else { ?>

<div id="gomy-options-error">

    <h3><?php echo __( 'Ninja Trouble', 'gomysites' ); ?></h3>
    
    <?php if ( $isgomyTheme && ( ! $meetsMinVersion ) ) { ?>
    <p><?php echo sprinf ( __( 'Your version of the gomyFramework (%s) does not yet support shortcodes. Shortcodes were introduced with version %s of the framework.', 'gomysites' ), $gomy_framework_version, $MIN_VERSION ); ?></p>
    
    <h4><?php echo __( 'What to do now?', 'gomysites' ); ?></h4>
    
    <p><?php echo __( 'Upgrading your theme, or rather the gomyFramework portion of it, will do the trick.', 'gomysites' ); ?></p>

	<p><?php echo sprintf( __( 'The framework is a collection of functionality that all gomysites have in common. In most cases you can update the framework even if you have modified your theme, because the framework resides in a separate location (under %s).', 'gomysites' ), '<code>/functions/</code>' ); ?></p>
	
	<p><?php echo sprintf ( __( 'There\'s a tutorial on how to do this on gomysites.com: %sHow to upgradeyour theme%s.', 'gomysites' ), '<a title="gomysites Tutorial" target="_blank" href="http://www.gomysites.com/2009/08/how-to-upgrade-your-theme/">', '</a>' ); ?></p>
	
	<p><?php echo __( '<strong>Remember:</strong> Every Ninja has a backup plan. Safe or not, always backup your theme before you update it or make changes to it.', 'gomysites' ); ?></p>

<?php } else { ?>

    <p><?php echo __( 'Looks like your active theme is not from gomysites. The shortcode generator only works with themes from gomysites.', 'gomysites' ); ?></p>
    
    <h4><?php echo __( 'What to do now?', 'gomysites' ); ?></h4>

	<p><?php echo __( 'Pick a fight: (1) If you already have a theme from gomysites, install and activate it or (2) if you don\'t yet have one of the awesome gomysites head over to the <a href="http://www.gomysites.com/themes/" target="_blank" title="gomysites Gallery">gomysites Gallery</a> and get one.', 'gomysites' ); ?></p>

<?php } ?>

<div style="float: right"><input type="button" id="gomy-btn-cancel"
	class="button" name="cancel" value="Cancel" accesskey="C" /></div>
</div>

<?php  } ?>

<script type="text/javascript" src="<?php echo $gomy_framework_url; ?>js/shortcode-generator/js/dialog-js.php"></script>

</div>

</body>
</html>