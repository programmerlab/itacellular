<?php

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Custom fields for WP write panel - gomysites_metabox_create
- gomysites_uploader_custom_fields
- gomysites_metabox_handle
- gomysites_metabox_add
- gomysites_metabox_header

-----------------------------------------------------------------------------------*/



/*-----------------------------------------------------------------------------------*/
// Custom fields for WP write panel
/*-----------------------------------------------------------------------------------*/

function gomysites_metabox_create($post,$callback) {
    global $post;

    $template_to_show = $callback['args'];

    $gomy_metaboxes = get_option( 'gomy_custom_template' );

    $seo_metaboxes = get_option( 'gomy_custom_seo_template' );

    if(empty($seo_metaboxes) AND $template_to_show == 'seo'){
    	return;
    }
    if(get_option( 'seo_gomy_hide_fields') != 'true' AND $template_to_show == 'seo'){
    	$gomy_metaboxes = $seo_metaboxes;
    }

    $output = '';
    $output .= '<table class="gomy_metaboxes_table">'."\n";
    foreach ($gomy_metaboxes as $gomy_metabox) {
    	$gomy_id = "gomysites_" . $gomy_metabox["name"];
    	$gomy_name = $gomy_metabox["name"];

    	if ($template_to_show == 'seo') {
    		$metabox_post_type_restriction = 'undefined';
    	} elseif (function_exists( 'gomysites_content_builder_menu')) {
    		$metabox_post_type_restriction = $gomy_metabox['cpt'][$post->post_type];
    	} else {
    		$metabox_post_type_restriction = 'undefined';
    	}

    	if ( ($metabox_post_type_restriction != '') && ($metabox_post_type_restriction == 'true') ) {
    		$type_selector = true;
    	} elseif ($metabox_post_type_restriction == 'undefined') {
    		$type_selector = true;
    	} else {
    		$type_selector = false;
    	}

   		$gomy_metaboxvalue = '';

    	if ($type_selector) {

    		if(
        	        $gomy_metabox['type'] == 'text'
			OR      $gomy_metabox['type'] == 'select'
			OR      $gomy_metabox['type'] == 'select2'
			OR      $gomy_metabox['type'] == 'checkbox'
			OR      $gomy_metabox['type'] == 'textarea'
			OR      $gomy_metabox['type'] == 'calendar'
			OR      $gomy_metabox['type'] == 'time'
			OR      $gomy_metabox['type'] == 'radio'
			OR      $gomy_metabox['type'] == 'images') {

        	    	$gomy_metaboxvalue = get_post_meta($post->ID,$gomy_name,true);

				}
				
				// Make sure slashes are stripped before output.
				foreach ( array( 'label', 'desc', 'std' ) as $k ) {
					if ( isset( $gomy_metabox[$k] ) && ( $gomy_metabox[$k] != '' ) ) {
						$gomy_metabox[$k] = stripslashes( $gomy_metabox[$k] );
					}
				}
				
        	    if ( $gomy_metaboxvalue == '' && isset( $gomy_metabox['std'] ) ) {

        	        $gomy_metaboxvalue = $gomy_metabox['std'];
        	    } 
        	    
				if($gomy_metabox['type'] == 'info'){

        	        $output .= "\t".'<tr style="background:#f8f8f8; font-size:11px; line-height:1.5em;">';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'. esc_attr( $gomy_id ) .'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td style="font-size:11px;">'.$gomy_metabox['desc'].'</td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }
        	    elseif($gomy_metabox['type'] == 'text'){

        	    	$add_class = ''; $add_counter = '';
        	    	if($template_to_show == 'seo'){$add_class = 'words-count'; $add_counter = '<span class="counter">0 characters, 0 words</span>';}
        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="gomy_input_text '.$add_class.'" type="'.$gomy_metabox['type'].'" value="'.esc_attr( $gomy_metaboxvalue ).'" name="'.$gomy_name.'" id="'.esc_attr( $gomy_id ).'"/>';
        	        $output .= '<span class="gomy_metabox_desc">'.$gomy_metabox['desc'] .' '. $add_counter .'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ($gomy_metabox['type'] == 'textarea'){

        	   		$add_class = ''; $add_counter = '';
        	    	if( $template_to_show == 'seo' ){ $add_class = 'words-count'; $add_counter = '<span class="counter">0 characters, 0 words</span>'; }
        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.$gomy_metabox.'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><textarea class="gomy_input_textarea '.$add_class.'" name="'.$gomy_name.'" id="'.esc_attr( $gomy_id ).'">' . esc_textarea(stripslashes($gomy_metaboxvalue)) . '</textarea>';
        	        $output .= '<span class="gomy_metabox_desc">'.$gomy_metabox['desc'] .' '. $add_counter.'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ($gomy_metabox['type'] == 'calendar'){

        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.$gomy_metabox.'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="gomy_input_calendar" type="text" name="'.$gomy_name.'" id="'.esc_attr( $gomy_id ).'" value="'.esc_attr( $gomy_metaboxvalue ).'">';
        	        $output .= '<span class="gomy_metabox_desc">'.$gomy_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ($gomy_metabox['type'] == 'time'){

        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="gomy_input_time" type="'.$gomy_metabox['type'].'" value="'.esc_attr( $gomy_metaboxvalue ).'" name="'.$gomy_name.'" id="'.esc_attr( $gomy_id ).'"/>';
        	        $output .= '<span class="gomy_metabox_desc">'.$gomy_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ($gomy_metabox['type'] == 'select'){

        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><select class="gomy_input_select" id="'.esc_attr( $gomy_id ).'" name="'. esc_attr( $gomy_name ) .'">';
        	        $output .= '<option value="">Select to return to default</option>';

        	        $array = $gomy_metabox['options'];

        	        if($array){

        	            foreach ( $array as $id => $option ) {
        	                $selected = '';

        	                if(isset($gomy_metabox['default']))  {
								if($gomy_metabox['default'] == $option && empty($gomy_metaboxvalue)){$selected = 'selected="selected"';}
								else  {$selected = '';}
							}

        	                if($gomy_metaboxvalue == $option){$selected = 'selected="selected"';}
        	                else  {$selected = '';}

        	                $output .= '<option value="'. esc_attr( $option ) .'" '. $selected .'>' . $option .'</option>';
        	            }
        	        }

        	        $output .= '</select><span class="gomy_metabox_desc">'.$gomy_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";
        	    }
        	    elseif ($gomy_metabox['type'] == 'select2'){

        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><select class="gomy_input_select" id="'.esc_attr( $gomy_id ).'" name="'. esc_attr( $gomy_name ) .'">';
        	        $output .= '<option value="">Select to return to default</option>';

        	        $array = $gomy_metabox['options'];

        	        if($array){

        	            foreach ( $array as $id => $option ) {
        	                $selected = '';

        	                if(isset($gomy_metabox['default']))  {
								if($gomy_metabox['default'] == $id && empty($gomy_metaboxvalue)){$selected = 'selected="selected"';}
								else  {$selected = '';}
							}

        	                if($gomy_metaboxvalue == $id){$selected = 'selected="selected"';}
        	                else  {$selected = '';}

        	                $output .= '<option value="'. esc_attr( $id ) .'" '. $selected .'>' . $option .'</option>';
        	            }
        	        }

        	        $output .= '</select><span class="gomy_metabox_desc">'.$gomy_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";
        	    }

        	    elseif ($gomy_metabox['type'] == 'checkbox'){

        	        if($gomy_metaboxvalue == 'true') { $checked = ' checked="checked"';} else {$checked='';}

        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input type="checkbox" '.$checked.' class="gomy_input_checkbox" value="true"  id="'.esc_attr( $gomy_id ).'" name="'. esc_attr( $gomy_name ) .'" />';
        	        $output .= '<span class="gomy_metabox_desc" style="display:inline">'.$gomy_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";
        	    }

        	    elseif ($gomy_metabox['type'] == 'radio'){

        	    $array = $gomy_metabox['options'];

        	    if($array){

        	    $output .= "\t".'<tr>';
        	    $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
        	    $output .= "\t\t".'<td>';

        	        foreach ( $array as $id => $option ) {

        	            if($gomy_metaboxvalue == $id) { $checked = ' checked';} else {$checked='';}

        	                $output .= '<input type="radio" '.$checked.' value="' . $id . '" class="gomy_input_radio"  name="'. esc_attr( $gomy_name ) .'" />';
        	                $output .= '<span class="gomy_input_radio_desc" style="display:inline">'. $option .'</span><div class="gomy_spacer"></div>';
        	            }
        	            $output .= "\t".'</tr>'."\n";
        	         }
        	    }
				elseif ($gomy_metabox['type'] == 'images')
				{

				$i = 0;
				$select_value = '';
				$layout = '';

				foreach ($gomy_metabox['options'] as $key => $option)
					 {
					 $i++;

					 $checked = '';
					 $selected = '';
					 if($gomy_metaboxvalue != '') {
					 	if ($gomy_metaboxvalue == $key) { $checked = ' checked'; $selected = 'gomy-meta-radio-img-selected'; }
					 }
					 else {
					 	if ($option['std'] == $key) { $checked = ' checked'; }
						elseif ($i == 1) { $checked = ' checked'; $selected = 'gomy-meta-radio-img-selected'; }
						else { $checked=''; }

					 }

						$layout .= '<div class="gomy-meta-radio-img-label">';
						$layout .= '<input type="radio" id="gomy-meta-radio-img-' . $gomy_name . $i . '" class="checkbox gomy-meta-radio-img-radio" value="'.esc_attr($key).'" name="'. $gomy_name.'" '.$checked.' />';
						$layout .= '&nbsp;' . esc_html($key) .'<div class="gomy_spacer"></div></div>';
						$layout .= '<img src="'.esc_url( $option ).'" alt="" class="gomy-meta-radio-img-img '. $selected .'" onClick="document.getElementById(\'gomy-meta-radio-img-'. esc_js($gomy_metabox["name"] . $i).'\').checked = true;" />';
					}

				$output .= "\t".'<tr>';
				$output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
				$output .= "\t\t".'<td class="gomy_metabox_fields">';
				$output .= $layout;
				$output .= '<span class="gomy_metabox_desc">'.$gomy_metabox['desc'].'</span></td>'."\n";
        	    $output .= "\t".'</tr>'."\n";

				}

        	    elseif($gomy_metabox['type'] == 'upload')
        	    {
					if(isset($gomy_metabox["default"])) $default = $gomy_metabox["default"];
					else $default = '';

        	    	// Add support for the gomysites Media Library-driven Uploader Module // 2010-11-09.
        	    	if ( function_exists( 'gomysites_medialibrary_uploader' ) ) {

        	    		$_value = $default;

        	    		$_value = get_post_meta( $post->ID, $gomy_metabox["name"], true );

        	    		$output .= "\t".'<tr>';
	    	            $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.$gomy_metabox["name"].'">'.$gomy_metabox['label'].'</label></th>'."\n";
	    	            $output .= "\t\t".'<td class="gomy_metabox_fields">'. gomysites_medialibrary_uploader( $gomy_metabox["name"], $_value, 'postmeta', $gomy_metabox["desc"], $post->ID );
	    	            $output .= '</td>'."\n";
	    	            $output .= "\t".'</tr>'."\n";

        	    	} else {

	    	            $output .= "\t".'<tr>';
	    	            $output .= "\t\t".'<th class="gomy_metabox_names"><label for="'.esc_attr( $gomy_id ).'">'.$gomy_metabox['label'].'</label></th>'."\n";
	    	            $output .= "\t\t".'<td class="gomy_metabox_fields">'. gomysites_uploader_custom_fields($post->ID,$gomy_name,$default,$gomy_metabox["desc"]);
	    	            $output .= '</td>'."\n";
	    	            $output .= "\t".'</tr>'."\n";

        	        } // End IF Statement

        	    }
        }	// End IF Statement
    }

    $output .= '</table>'."\n\n";
    echo $output;
}



/*-----------------------------------------------------------------------------------*/
// gomysites_uploader_custom_fields
/*-----------------------------------------------------------------------------------*/

function gomysites_uploader_custom_fields($pID,$id,$std,$desc){

    // Start Uploader
    $upload = get_post_meta( $pID, $id, true);
	$href = cleanSource($upload);
	$uploader = '';
    $uploader .= '<input class="gomy_input_text" name="'.$id.'" type="text" value="'.esc_attr($upload).'" />';
    $uploader .= '<div class="clear"></div>'."\n";
    $uploader .= '<input type="file" name="attachement_'.$id.'" />';
    $uploader .= '<input type="submit" class="button button-highlighted" value="Save" name="save"/>';
    if ( $href )
		$uploader .= '<span class="gomy_metabox_desc">'.$desc.'</span></td>'."\n".'<td class="gomy_metabox_image"><a href="'. $upload .'"><img src="'.get_template_directory_uri().'/functions/thumb.php?src='.$href.'&w=150&h=80&zc=1" alt="" /></a>';

return $uploader;
}



/*-----------------------------------------------------------------------------------*/
// gomysites_metabox_handle
/*-----------------------------------------------------------------------------------*/

function gomysites_metabox_handle(){

    $pID = '';
    global $globals, $post;

    $gomy_metaboxes = get_option( 'gomy_custom_template' );

    $seo_metaboxes = get_option( 'gomy_custom_seo_template' );

    if(!empty($seo_metaboxes) AND get_option( 'seo_gomy_hide_fields') != 'true'){
    	$gomy_metaboxes = array_merge($gomy_metaboxes,$seo_metaboxes);
    }

    // Sanitize post ID.

    if( isset( $_POST['post_ID'] ) ) {

		$pID = intval( $_POST['post_ID'] );

    } // End IF Statement

    // Don't continue if we don't have a valid post ID.

    if ( $pID == 0 ) {

    	return;

    } // End IF Statement

    $upload_tracking = array();

    if ( isset( $_POST['action'] ) && $_POST['action'] == 'editpost' ) {

        foreach ($gomy_metaboxes as $gomy_metabox) { // On Save.. this gets looped in the header response and saves the values submitted
            if($gomy_metabox['type'] == 'text'
			OR $gomy_metabox['type'] == 'calendar'
			OR $gomy_metabox['type'] == 'time'
			OR $gomy_metabox['type'] == 'select'
			OR $gomy_metabox['type'] == 'select2'
			OR $gomy_metabox['type'] == 'radio'
			OR $gomy_metabox['type'] == 'checkbox'
			OR $gomy_metabox['type'] == 'textarea'
			OR $gomy_metabox['type'] == 'images' ) // Normal Type Things...
            {

				$var = $gomy_metabox["name"];

				if ( isset( $_POST[$var] ) ) {

					// Sanitize the input.
					$posted_value = '';
					$posted_value = $_POST[$var];

				    // Get the current value for checking in the script.
				    $current_value = '';
				    $current_value = get_post_meta( $pID, $var, true );

					 // If it doesn't exist, add the post meta.
					if(get_post_meta( $pID, $var ) == "") {

						add_post_meta( $pID, $var, $posted_value, true );

					}
					// Otherwise, if it's different, update the post meta.
					elseif( $posted_value != get_post_meta( $pID, $var, true ) ) {

						update_post_meta( $pID, $var, $posted_value );

					}
					// Otherwise, if no value is set, delete the post meta.
					elseif($posted_value == "") {

						delete_post_meta( $pID, $var, get_post_meta( $pID, $var, true ) );

					} // End IF Statement

					/*
				    // If it doesn't exist, add the post meta.
					if ( $current_value == "" && $posted_value != '' ) {

						update_post_meta( $pID, $var, $posted_value );

					// Otherwise, if it's different, update the post meta.
					} elseif ( ( $posted_value != '' ) && ( $posted_value != $current_value ) ) {

						update_post_meta( $pID, $var, $posted_value );

					// Otherwise, if no value is set, delete the post meta.
					} elseif ( $posted_value == "" && $current_value != '' ) {

						delete_post_meta($pID, $var, $current_value );

					} // End IF Statement
					*/

				} elseif ( ! isset( $_POST[$var] ) && $gomy_metabox['type'] == 'checkbox' ) {

					update_post_meta( $pID, $var, 'false' );

				} else {

					delete_post_meta( $pID, $var, $current_value ); // Deletes check boxes OR no $_POST

				} // End IF Statement

            } elseif( $gomy_metabox['type'] == 'upload' ) { // So, the upload inputs will do this rather

				$id = $gomy_metabox['name'];
				$override['action'] = 'editpost';

			    if(!empty($_FILES['attachement_'.$id]['name'])){ //New upload
			    $_FILES['attachement_'.$id]['name'] = preg_replace( '/[^a-zA-Z0-9._\-]/', '', $_FILES['attachement_'.$id]['name']);
			           $uploaded_file = wp_handle_upload($_FILES['attachement_' . $id ],$override);
			           $uploaded_file['option_name']  = $gomy_metabox['label'];
			           $upload_tracking[] = $uploaded_file;
			           update_post_meta( $pID, $id, $uploaded_file['url'] );

			    } elseif ( empty( $_FILES['attachement_'.$id]['name'] ) && isset( $_POST[ $id ] ) ) {

			       	// Sanitize the input.
					$posted_value = '';
					$posted_value = $_POST[$id];

			        update_post_meta($pID, $id, $posted_value);

			    } elseif ( $_POST[ $id ] == '' )  {

			    	delete_post_meta( $pID, $id, get_post_meta( $pID, $id, true ) );

			    } // End IF Statement

			} // End IF Statement

               // Error Tracking - File upload was not an Image
               update_option( 'gomy_custom_upload_tracking', $upload_tracking );

            } // End FOREACH Loop

        } // End IF Statement

} // End gomysites_metabox_handle()

/*-----------------------------------------------------------------------------------*/
// gomysites_metabox_add
/*-----------------------------------------------------------------------------------*/

function gomysites_metabox_add() {
	$seo_metaboxes = get_option( 'gomy_custom_seo_template' );
	$seo_post_types = array( 'post','page' );
	if(defined( 'SEOPOSTTYPES')){
		$seo_post_types_update = unserialize( constant( 'SEOPOSTTYPES') );
	}

	if(!empty($seo_post_types_update)){
		$seo_post_types = $seo_post_types_update;
	}

	$gomy_metaboxes = get_option( 'gomy_custom_template' );

    if ( function_exists( 'add_meta_box') ) {

    	if ( function_exists( 'get_post_types') ) {
    		$custom_post_list = get_post_types();

    		// Get the theme name for use in multiple meta boxes.
    		$theme_name = get_option( 'gomy_themename' );

			foreach ($custom_post_list as $type){

				$settings = array(
									'id' => 'gomysites-settings',
									'title' => $theme_name . __( ' Custom Settings', 'gomysites' ),
									'callback' => 'gomysites_metabox_create',
									'page' => $type,
									'priority' => 'normal',
									'callback_args' => ''
								);

				// Allow child themes/plugins to filter these settings.
				$settings = apply_filters( 'gomysites_metabox_settings', $settings, $type, $settings['id'] );

				if ( ! empty( $gomy_metaboxes ) ) {
					add_meta_box( $settings['id'], $settings['title'], $settings['callback'], $settings['page'], $settings['priority'], $settings['callback_args'] );
				}

				//if(!empty($gomy_metaboxes)) Temporarily Removed

				if(array_search($type, $seo_post_types) !== false){
					if(get_option( 'seo_gomy_hide_fields') != 'true'){
						add_meta_box( 'gomysites-seo', $theme_name . ' SEO Settings','gomysites_metabox_create',$type,'normal','high','seo' );
					}
				}
			}
    	} else {
    		add_meta_box( 'gomysites-settings', $theme_name . ' Custom Settings','gomysites_metabox_create','post','normal' );
        	add_meta_box( 'gomysites-settings', $theme_name . ' Custom Settings','gomysites_metabox_create','page','normal' );
        	if(get_option( 'seo_gomy_hide_fields') != 'true'){
        		add_meta_box( 'gomysites-seo', $theme_name . ' SEO Settings','gomysites_metabox_create','post','normal','high','seo' );
        		add_meta_box( 'gomysites-seo', $theme_name . ' SEO Settings','gomysites_metabox_create','page','normal','high','seo' );
    		}
    	}

    }
}

/*-----------------------------------------------------------------------------------*/
// gomysites_metabox_header
/*-----------------------------------------------------------------------------------*/

function gomysites_metabox_header(){
?>
<script type="text/javascript">

    jQuery(document).ready(function(){

        jQuery( 'form#post').attr( 'enctype','multipart/form-data' );
        jQuery( 'form#post').attr( 'encoding','multipart/form-data' );

         //JQUERY DATEPICKER
		jQuery( '.gomy_input_calendar').each(function (){
			jQuery( '#' + jQuery(this).attr( 'id')).datepicker({showOn: 'button', buttonImage: '<?php echo get_template_directory_uri(); ?>/functions/images/calendar.gif', buttonImageOnly: true});
		});

		//JQUERY TIME INPUT MASK
		jQuery( '.gomy_input_time').each(function (){
			jQuery( '#' + jQuery(this).attr( 'id')).mask( "99:99" );
		});

		//JQUERY CHARACTER COUNTER
		jQuery( '.words-count').each(function(){
			var s = ''; var s2 = '';
		    var length = jQuery(this).val().length;
		    var w_length = jQuery(this).val().split(/\b[\s,\.-:;]*/).length;
			
		    if(length != 1) { s = 's';}
		    if(w_length != 1){ s2 = 's';}
		    if(jQuery(this).val() == ''){ s2 = 's'; w_length = '0';}

		    jQuery(this).parent().find( '.counter').html( length + ' character'+ s + ', ' + w_length + ' word' + s2);

		    jQuery(this).keyup(function(){
		    var s = ''; var s2 = '';
		        var new_length = jQuery(this).val().length;
		        var word_length = jQuery(this).val().split(/\b[\s,\.-:;]*/).length;

		        if(new_length != 1) { s = 's';}
		        if(word_length != 1){ s2 = 's'}
		        if(jQuery(this).val() == ''){ s2 == 's'; word_length = '0';}

		        jQuery(this).parent().find( '.counter').html( new_length + ' character' + s + ', ' + word_length + ' word' + s2);
		    });
		});

        jQuery( '.gomy_metaboxes_table th:last, .gomy_metaboxes_table td:last').css( 'border','0' );
        var val = jQuery( 'input#title').attr( 'value' );
        if(val == ''){
        jQuery( '.gomy_metabox_fields .button-highlighted').after( "<em class='gomy_red_note'>Please add a Title before uploading a file</em>" );
        };
		jQuery( '.gomy-meta-radio-img-img').click(function(){
				jQuery(this).parent().find( '.gomy-meta-radio-img-img').removeClass( 'gomy-meta-radio-img-selected' );
				jQuery(this).addClass( 'gomy-meta-radio-img-selected' );

			});
			jQuery( '.gomy-meta-radio-img-label').hide();
			jQuery( '.gomy-meta-radio-img-img').show();
			jQuery( '.gomy-meta-radio-img-radio').hide();
        <?php //Errors
        $error_occurred = false;
        $upload_tracking = get_option( 'gomy_custom_upload_tracking' );
        if(!empty($upload_tracking)){
        $output = '<div style="clear:both;height:20px;"></div><div class="errors"><ul>' . "\n";
            $error_shown == false;
            foreach($upload_tracking as $array )
            {
                 if(array_key_exists( 'error', $array)){
                        $error_occurred = true;
                        ?>
                        jQuery( 'form#post').before( '<div class="updated fade"><p>gomysites Upload Error: <strong><?php echo $array['option_name'] ?></strong> - <?php echo $array['error'] ?></p></div>' );
                        <?php
                }
            }
        }

        delete_option( 'gomy_upload_custom_errors' );
        ?>
    });

</script>
<style type="text/css">
.gomy_input_text { margin:0 0 10px 0; background:#f4f4f4; color:#444; width:80%; font-size:11px; padding: 5px;}
.gomy_input_select { margin:0 0 10px 0; background:#f4f4f4; color:#444; width:60%; font-size:11px; padding: 5px;}
.gomy_input_checkbox { margin:0 10px 0 0; }
.gomy_input_radio { margin:0 10px 0 0; }
.gomy_input_radio_desc { font-size: 12px; color: #666 ; }
.gomy_input_calendar { margin:0 0 10px 0; }
.gomy_spacer { display: block; height:5px}
.gomy_metabox_desc { font-size:10px; color:#aaa; display:block}
.gomy_metaboxes_table{ border-collapse:collapse; width:100%}
.gomy_metaboxes_table th,
.gomy_metaboxes_table td{ border-bottom:1px solid #ddd; padding:10px 10px;text-align: left; vertical-align:top}
.gomy_metabox_names { width:20%}
.gomy_metabox_fields { width:70%}
.gomy_metabox_image { text-align: right;}
.gomy_red_note { margin-left: 5px; color: #c77; font-size: 10px;}
.gomy_input_textarea { width:80%; height:120px;margin:0 0 10px 0; background:#f0f0f0; color:#444;font-size:11px;padding: 5px;}
.gomy-meta-radio-img-img { border:3px solid #dedede; margin:0 5px 10px 0; display:none; cursor:pointer; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;}
.gomy-meta-radio-img-img:hover, .gomy-meta-radio-img-selected { border:3px solid #aaa; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; }
.gomy-meta-radio-img-label { font-size:12px}
.gomy_metabox_desc span.counter { color:green!important }
.gomy_metabox_fields .controls input.upload { width:280px; padding-bottom:6px; }
.gomy_metabox_fields .controls input.upload_button{ float:right; width:auto; border-color:#BBBBBB; cursor:pointer; height:16px; }
.gomy_metabox_fields .controls input.upload_button:hover { width:auto; border-color:#666666; color:#000; }
.gomy_metabox_fields .screenshot{margin:10px 0;float:left;margin-left:1px;position:relative;width:344px;}
.gomy_metabox_fields .screenshot img{-moz-border-radius:4px;-webkit-border-radius:4px;-border-radius:4px;background:#FAFAFA;float:left;max-width:334px;border-color:#CCC #EEE #EEE #CCC;border-style:solid;border-width:1px;padding:4px;}
.gomy_metabox_fields .screenshot .mlu_remove{background:url( "<?php echo get_template_directory_uri(); ?>/functions/images/ico-delete.png") no-repeat scroll 0 0 transparent;border:medium none;bottom:-4px;display:block;float:left;height:16px;position:absolute;left:-4px;text-indent:-9999px;width:16px;padding:0;}
.gomy_metabox_fields .upload {background:none repeat scroll 0 0 #F4F4F4;color:#444444;font-size:11px;margin:0 0 10px;padding:5px;width:70%;}
.gomy_metabox_fields .upload_button {-moz-border-radius:4px; -webkit-border-radius:4px;-border-radius:4px;}
.gomy_metabox_fields .screenshot .no_image .file_link {margin-left: 20px;}
.gomy_metabox_fields .screenshot .no_image .mlu_remove {bottom: 0px;}
</style>
<?php
 echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/functions/css/jquery-ui-datepicker.css" />';
}


function gomy_custom_enqueue($hook) {
  	if ($hook == 'post.php' OR $hook == 'post-new.php' OR $hook == 'page-new.php' OR $hook == 'page.php') {
		add_action( 'admin_head', 'gomysites_metabox_header' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_register_script( 'jquery-ui-datepicker', get_template_directory_uri() . '/functions/js/ui.datepicker.js', array( 'jquery-ui-core' ));
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_register_script( 'jquery-input-mask', get_template_directory_uri() . '/functions/js/jquery.maskedinput-1.2.2.js', array( 'jquery' ));
		wp_enqueue_script( 'jquery-input-mask' );
  	}
}

add_action( 'admin_enqueue_scripts', 'gomy_custom_enqueue', 10, 1 );
add_action( 'edit_post', 'gomysites_metabox_handle' );
add_action( 'admin_menu', 'gomysites_metabox_add' ); // Triggers gomysites_metabox_create
?>