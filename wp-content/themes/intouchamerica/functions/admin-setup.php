<?php

/*-------------------------------------------------------------------------------------

TABLE OF CONTENTS

- Redirect to "Theme Options" screen (hooked onto gomy_theme_activate at 10).
- Flush rewrite rules to refresh permalinks for custom post types, etc.
- Show Options Panel after activate
- Admin Backend
	- Setup Custom Navigation
- Output HEAD - gomysites_wp_head()
	- Output alternative stylesheet
	- Output custom favicon
	- Load textdomains
	- Output CSS from standarized styling options
	- Output shortcodes stylesheet
	- Output custom.css
- Post Images from WP2.9+ integration
- Enqueue comment reply script

-------------------------------------------------------------------------------------*/

define( 'THEME_FRAMEWORK', 'gomysites' );

/*-----------------------------------------------------------------------------------*/
/* Redirect to "Theme Options" screen (hooked onto gomy_theme_activate at 10). */
/*-----------------------------------------------------------------------------------*/
add_action( 'gomy_theme_activate', 'gomy_themeoptions_redirect', 10 );

function gomy_themeoptions_redirect () {
	// Do redirect
	header( 'Location: ' . admin_url() . 'admin.php?page=gomysites' );
} // End gomy_themeoptions_redirect()

/*-----------------------------------------------------------------------------------*/
/* Flush rewrite rules to refresh permalinks for custom post types, etc. */
/*-----------------------------------------------------------------------------------*/

function gomy_flush_rewriterules () {
	flush_rewrite_rules();
} // End gomy_flush_rewriterules()

/*-----------------------------------------------------------------------------------*/
/* Add default options and show Options Panel after activate  */
/*-----------------------------------------------------------------------------------*/
global $pagenow;

if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
	// Call action that sets.
	add_action( 'admin_head','gomy_option_setup' );
	
	// Flush rewrite rules.
	add_action( 'admin_head', 'gomy_flush_rewriterules', 9 );
	
	// Custom action for theme-setup (redirect is at priority 10).
	do_action( 'gomy_theme_activate' );
}


if ( ! function_exists( 'gomy_option_setup' ) ) {
	function gomy_option_setup(){

		//Update EMPTY options
		$gomy_array = array();
		add_option( 'gomy_options', $gomy_array );

		$template = get_option( 'gomy_template' );
		$saved_options = get_option( 'gomy_options' );

		foreach ( (array) $template as $option ) {
			if ($option['type'] != 'heading'){
				$id = $option['id'];
				$std = isset( $option['std'] ) ? $option['std'] : NULL;
				$db_option = get_option($id);
				if (empty($db_option)){
					if (is_array($option['type'])) {
						foreach ($option['type'] as $child){
							$c_id = $child['id'];
							$c_std = $child['std'];
							$db_option = get_option($c_id);
							if (!empty($db_option)){
								update_option($c_id,$db_option);
								$gomy_array[$id] = $db_option;
							} else {
								$gomy_array[$c_id] = $c_std;
							}
						}
					} else {
						update_option($id,$std);
						$gomy_array[$id] = $std;
					}
				} else { //So just store the old values over again.
					$gomy_array[$id] = $db_option;
				}
			}
		}
		
		// Allow child themes/plugins to filter here.
		$gomy_array = apply_filters( 'gomy_options_array', $gomy_array );
		
		update_option( 'gomy_options', $gomy_array );
	}
}

/*-----------------------------------------------------------------------------------*/
/* Admin Backend */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomysites_admin_head' ) ) {
	function gomysites_admin_head() {
	    //Setup Custom Navigation Menu
		if ( function_exists( 'gomy_custom_navigation_setup' ) ) {
			gomy_custom_navigation_setup();
		}
	}
}
add_action( 'admin_head', 'gomysites_admin_head', 10 );


/*-----------------------------------------------------------------------------------*/
/* Output HEAD - gomysites_wp_head */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomysites_wp_head' ) ) {
	function gomysites_wp_head() {

		do_action( 'gomysites_wp_head_before' );

		// Output alternative stylesheet
//		if ( function_exists( 'gomy_output_alt_stylesheet' ) )
//			gomy_output_alt_stylesheet();

		// Output custom favicon
		if ( function_exists( 'gomy_output_custom_favicon' ) )
			gomy_output_custom_favicon();

		// Output CSS from standarized styling options
		if ( function_exists( 'gomy_head_css' ) )
			gomy_head_css();

		// Output shortcodes stylesheet
		if ( function_exists( 'gomy_shortcode_stylesheet' ) )
			gomy_shortcode_stylesheet();

		// Output custom.css
//		if ( function_exists( 'gomy_output_custom_css' ) )
//			gomy_output_custom_css();
			
		do_action( 'gomysites_wp_head_after' );
	} // End gomysites_wp_head()
}
add_action( 'wp_head', 'gomysites_wp_head', 10 );

/*-----------------------------------------------------------------------------------*/
/* Output alternative stylesheet - gomy_output_alt_stylesheet */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomy_output_alt_stylesheet' ) ) {
	function gomy_output_alt_stylesheet() {

		$style = '';

		if ( isset( $_REQUEST['style'] ) ) {
			// Sanitize requested value.
			$requested_style = strtolower( strip_tags( trim( $_REQUEST['style'] ) ) );
			$style = $requested_style;
		}

		echo "<!-- Alt Stylesheet -->\n";
		if ($style != '') {
			echo '<link href="'. get_template_directory_uri() . '/styles/'. $style . '.css" rel="stylesheet" type="text/css" />' . "\n\n";
		} else {
			$style = get_option( 'gomy_alt_stylesheet' );
			if( $style != '' ) {
				// Sanitize value.
				$style = strtolower( strip_tags( trim( $style ) ) );
				echo '<link href="'. get_template_directory_uri() . '/styles/'. $style .'" rel="stylesheet" type="text/css" />' . "\n\n";
			} else {
				echo '<link href="'. get_template_directory_uri() . '/styles/default.css" rel="stylesheet" type="text/css" />' . "\n\n";
			}
		}

	} // End gomy_output_alt_stylesheet()
}

/*-----------------------------------------------------------------------------------*/
/* Output favicon link - gomy_custom_favicon() */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomy_output_custom_favicon' ) ) {
	function gomy_output_custom_favicon() {
		// Favicon
		$favicon = '';
		$favicon = get_option( 'gomy_custom_favicon' );
		
		// Allow child themes/plugins to filter here.
		$favicon = apply_filters( 'gomy_custom_favicon', $favicon );
		if( $favicon != '' ) {
			echo "<!-- Custom Favicon -->\n";
	        echo '<link rel="shortcut icon" href="' .  esc_url( $favicon )  . '"/>' . "\n\n";
	    }
	} // End gomy_output_custom_favicon()
}

/*-----------------------------------------------------------------------------------*/
/* Load textdomain - gomy_load_textdomain() */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomy_load_textdomain' ) ) {
	function gomy_load_textdomain() {

		load_theme_textdomain( 'gomysites' );
		load_theme_textdomain( 'gomysites', get_template_directory() . '/lang' );
		if ( function_exists( 'load_child_theme_textdomain' ) )
			load_child_theme_textdomain( 'gomysites' );

	}
}

add_action( 'init', 'gomy_load_textdomain', 10 );

/*-----------------------------------------------------------------------------------*/
/* Output CSS from standarized options */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomy_head_css' ) ) {
	function gomy_head_css() {

		$output = '';
		$text_title = get_option( 'gomy_texttitle' );
		$tagline = get_option( 'gomy_tagline' );
	    $custom_css = get_option( 'gomy_custom_css' );

		$template = get_option( 'gomy_template' );
		if (is_array($template)) {
			foreach($template as $option){
				if(isset($option['id'])){
					if($option['id'] == 'gomy_texttitle') {
						// Add CSS to output
						if ( $text_title == "true" ) {
							$output .= '#logo img { display:none; } .site-title { display:block!important; }' . "\n";
							if ( $tagline == "false" )
								$output .= '.site-description { display:none!important; }' . "\n";
							else
								$output .= '.site-description { display:block!important; }' . "\n";
						}
					}
				}
			}
		}

		if ( $custom_css <> '' ) {
			$output .= $custom_css . "\n";
		}

		// Output styles
		if ( $output <> '' ) {
			$output = strip_tags($output);
			echo "<!-- Options Panel Custom CSS -->\n";
			$output = "<style type=\"text/css\">\n" . $output . "</style>\n\n";
			echo stripslashes( $output );
		}

	} // End gomy_head_css()
}



/*-----------------------------------------------------------------------------------*/
/* Output custom.css - gomy_custom_css() */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomy_output_custom_css' ) ) {
	function gomy_output_custom_css() {

		// Custom.css insert
		echo "<!-- Custom Stylesheet -->\n";
		echo '<link href="'. get_template_directory_uri() . '/custom.css" rel="stylesheet" type="text/css" />' . "\n";

	} // End gomy_output_custom_css()
}

/*-----------------------------------------------------------------------------------*/
/* Post Images from WP2.9+ integration /*
/*-----------------------------------------------------------------------------------*/
if( function_exists( 'add_theme_support' ) ) {
	if( get_option( 'gomy_post_image_support' ) == 'true' ) {
		add_theme_support( 'post-thumbnails' );
		// set height, width and crop if dynamic resize functionality isn't enabled
		if ( get_option( 'gomy_pis_resize' ) != 'true' ) {
			$thumb_width = get_option( 'gomy_thumb_w' );
			$thumb_height = get_option( 'gomy_thumb_h' );
			$single_width = get_option( 'gomy_single_w' );
			$single_height = get_option( 'gomy_single_h' );
			$hard_crop = get_option( 'gomy_pis_hard_crop' );
			if($hard_crop == 'true') { $hard_crop = true; } else { $hard_crop = false; }
			set_post_thumbnail_size( $thumb_width, $thumb_height, $hard_crop ); // Normal post thumbnails
			add_image_size( 'single-post-thumbnail', $single_width, $single_height, $hard_crop );
		}
	}
}


/*-----------------------------------------------------------------------------------*/
/* Enqueue comment reply script */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'gomy_comment_reply' ) ) {
	function gomy_comment_reply() {
		if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	} // End gomy_comment_reply()
}
add_action( 'get_header', 'gomy_comment_reply', 10 );
?>