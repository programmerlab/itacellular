<?php get_header(); ?>

	<div id="title-container" class="col-full post">
		<h1 class="title"><?php _e( 'Error 404 - Page not found!', 'gomysites' ) ?></h1>
		<?php include( get_template_directory() . '/search-form.php' ); ?>
	</div>
	
	
</div><!-- end top section -->	

	<?php if ( $gomy_options[ 'gomy_breadcrumbs_show' ] == 'true' ) { ?>
	<div id="breadcrumb-section"><!-- start breadcrumb container -->
		
		<div class="col-full">
			<div id="breadcrumbs">
				<?php gomy_breadcrumbs(); ?>
			</div><!-- end breadcrumbs -->
				
	        <div class="fix"></div>
        </div>
        
	</div><!-- end breadcrumb container-->  	
	<?php } ?> 

	
<div id="main-content"><!-- start main content area -->

    <div class="col-full"><!--start content-->
		 	
		<div id="main" class="fullwidth"><!--start main-->
			           		                                                                                
            <div class="entry">
            	<p><?php _e( 'The page you trying to reach does not exist, or has been moved. Please use the menus or the search box to find what you are looking for.', 'gomysites' ) ?></p>
            </div>
	                                                
		</div><!-- end main -->
	
    </div><!-- end col-full -->

</div><!-- main content -->	
	
<?php get_footer(); ?>