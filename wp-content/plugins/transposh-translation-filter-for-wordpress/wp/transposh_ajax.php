<?php

/*
 * Transposh v0.9.7.2
 * http://transposh.org/
 *
 * Copyright 2015, Team Transposh
 * Licensed under the GPL Version 2 or higher.
 * http://transposh.org/license
 *
 * Date: Wed, 29 Jul 2015 00:56:51 +0300
 */

/*
 * This file handles various AJAX needs of our plugin
 */

echo "This file was removed in 0.8.0 and is only kept for debugging";

?>